/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-18 14:20:28
 * @LastEditTime: 2019-09-04 16:51:25
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { Provider } from '@tarojs/mobx'
import Index from './pages/index'

import store from './store/index'
// import "./custom-style/custom-variables.scss";
import './app.scss'
// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }



class App extends Component {
  config = {
    pages: [
      "pages/index/index",
      "pages/city/index",
      "pages/case/index",
      "pages/hotel/index",
      "pages/product/index",
      "pages/car/index",
      "pages/user/index",
      "pages/door/index",
      "pages/login/index",
      "pages/hoteldetail/index",
      "pages/cardetail/index",
      "pages/casedetail/index",
      "pages/productdetail/index",
      "pages/orderstore/index",
      "pages/orderlist/index",
      "pages/map/index",
      "pages/activity/index",
      "pages/jsqpage/index",
      "pages/card/index",
      "pages/mycard/index",
      "pages/cardlist/index",
      "pages/cardinfo/index",
      "pages/date/index",
    ],
    window: {
      backgroundTextStyle: "light",
      navigationBarBackgroundColor: "#fff",
      navigationBarTitleText: "WeChat",
      navigationBarTextStyle: "black"
    },
    tabBar: {
      color: "#707070",
      selectedColor: "#ff6699",
      backgroundColor: "#fafafa",
      borderStyle: "black",
      list: [
        {
          pagePath: "pages/index/index",
          iconPath: "./assets/images/home.png",
          selectedIconPath: "./assets/images/home-active.png",
          text: "首页"
        },
        {
          pagePath: "pages/product/index",
          iconPath: "./assets/images/wedding.png",
          selectedIconPath: "./assets/images/wedding-active.png",
          text: "婚庆"
        },
        {
          pagePath: "pages/hotel/index",
          iconPath: "./assets/images/hotel.png",
          selectedIconPath: "./assets/images/hotel-active.png",
          text: "酒店"
        },
        {
          pagePath: "pages/car/index",
          iconPath: "./assets/images/car.png",
          selectedIconPath: "./assets/images/car-active.png",
          text: "婚车"
        },
        {
          pagePath: "pages/user/index",
          iconPath: "./assets/images/user.png",
          selectedIconPath: "./assets/images/user-active.png",
          text: "我的"
        }
      ]
    }
  };
  state = {}
  componentWillMount() {
    if (process.env.TARO_ENV === "weapp") {
      // this.autoUpdate();
    }
    const data = Taro.getStorageSync("_currentcity");
    if (data) {
      const value = JSON.parse(data);
      store.counterStore.gotCurrentCity(value);
    } else {
      Taro.navigateTo({
        url: "/pages/city/index"
      });
    }
    const user = Taro.getStorageSync("uu_user2030");
    if (user) {
      const value = JSON.parse(user);
      store.counterStore.gotUserInfo(value);
    }
  }
  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}

  componentDidCatchError() {}
  autoUpdate() {
    // 获取小程序更新机制兼容
    if (Taro.canIUse("getUpdateManager")) {
      // 检查小程序是否有新版本发布
      const updateManager = Taro.getUpdateManager();
      updateManager.onCheckForUpdate(res => {
        // 请求完新版本信息的回调
        console.log('请求完新版本信息的回调')
        if (res.hasUpdate) {
          console.log('有新版本')
          // 小程序有新版本，则静默下载新版本，做好更新准备
          updateManager.onUpdateReady(() => {
            Taro.showModal({
              title: "更新提示",
              content: "新版本已经准备好，请重启应用",
              showCancel: false
            }).then(res => {
              if (res.confirm) {
                updateManager.applyUpdate();
              }
            });
          });
          // 新版本下载失败
          updateManager.onUpdateFailed(function() {
            console.log('失败')
            Taro.showModal({
              title: "已经有新版本了哟~",
              content: "新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~"
            });
          });
        }
      });
    } else {
      Taro.showModal({
        title: "提示",
        content: "当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试"
      });
    }
  }
  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    );
  }
}

Taro.render(<App />, document.getElementById('app'))
