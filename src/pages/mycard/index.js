import Taro, { Component } from '@tarojs/taro'
import { View, Image, ScrollView} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { getWindowHeight } from '../../utils/style.js'
import './index.scss'

let baseurl = 'https://www.uuwed.com/weddingcard/card1'
@inject('counterStore')
@observer
class Index extends Component {

  config = {
    navigationBarTitleText: '我的请帖'
  }

  componentWillMount () { }

  componentWillReact () {
    
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  handleClick () {
    Taro.navigateTo({
      url: '/pages/cardlist/index'
    })
  }
  handleCat () {
    Taro.navigateTo({
      url: '/pages/card/index'
    })
  }
  handleEdit () {
    Taro.navigateTo({
      url: '/pages/cardinfo/index'
    })
  }
  render () {
    return (
      <View className='page card-page'>
         <ScrollView
          scrollY
          className="card-content"
          style={{ height: getWindowHeight(false, 60)}}
        >
          <View className="card-ul-box">
            <View className="card-ul">
              <View className="card-li">
                <Image src={`${baseurl}/page1.png`} className="card-li-image"/>
                <View className="card-li-operation">
                  <View className="card-li-btn card-li-cat" onClick={this.handleCat.bind(this)}>查看</View>
                  <View className="card-li-btn card-li-edit" onClick={this.handleEdit.bind(this)}>编辑</View>
                </View>
              </View>
              <View className="card-li">
                <Image src={`${baseurl}/page1.png`} className="card-li-image"/>
              </View>
              <View className="card-li">
                <Image src={`${baseurl}/page1.png`} className="card-li-image"/>
              </View>
            </View>
          </View>
        </ScrollView>
        <View className="make-btn-box">
          <View className="make-btn" onClick={this.handleClick.bind(this)}>立即制作</View>
        </View>
      </View>
    )
  }
}

export default Index 
