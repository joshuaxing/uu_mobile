/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-23 13:39:40
 * @LastEditTime: 2019-08-22 09:48:00
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, Swiper, SwiperItem} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import Header from '../../components/header/index.js'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class Door extends Component {
  tableliHeight = 340
  config = {
    navigationBarTitleText: '直营店铺'
  }
  navList = [
    {id: 0, name: '全部'},
    {id: 2, name: '直营'},
    {id: 1, name: '技术授权'}
  ]
  state = {
    totalpage: 0,
    alltotalpage: 0,
    chaintotalpage: 0,
    jointotalpage: 0,
    page: 1,
    limit: 8,
    type: 0,
    nodata: true,
    loading: false,
    hasMore: true,
    result: [],
    resultAll: [],
    resultChain: [],
    resultJoin: [],
    tableHeight: 30,
    tableIndex: 0
  }

  componentWillMount () {
    this.gotData()
  }

  componentWillReact () {
    this.setState({
      result: [],
      resultAll: [],
      resultChain: [],
      resultJoin: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  handleType (item) {
    const { resultAll, resultChain, resultJoin, alltotalpage, chaintotalpage, jointotalpage} = this.state;

    if (item.id === 0) {
      !resultAll.length ?
        this.setState({
          tableIndex: 0,
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          tableHeight: (resultAll.length * this.tableliHeight - 15),
          result: resultAll,
          tableIndex: 0,
          type: item.id,
          totalpage: alltotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    } else if (item.id === 2) {
      !resultChain.length ?
        this.setState({
          tableIndex: 1,
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          tableHeight: (resultChain.length * this.tableliHeight - 15),
          result: resultChain,
          tableIndex: 1,
          type: item.id,
          totalpage: chaintotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    } else {
      !resultJoin.length ?
        this.setState({
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          tableIndex: 2,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          tableHeight: (resultJoin.length * this.tableliHeight - 15),
          result: resultJoin,
          tableIndex: 2,
          type: item.id,
          totalpage: jointotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    }
  }
  gotData () {
    //列表数据
    const { counterStore: { currentcity } } = this.props
    const { page, limit, hasMore, loading, type} = this.state;
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true })
    http('/app/merchantlist.do', {
      cityid: currentcity.id,
      areaid: currentcity.areaid,
      page: page,
      limit: limit,
      type: type
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (type === 0) {
          this.setState({
            resultAll: alldata,
            alltotalpage: totalpage
          })
        } else if (type === 2) {
          this.setState({
            resultChain: alldata,
            chaintotalpage: totalpage
          })
        } else {
          this.setState({
            resultJoin: alldata,
            jointotalpage: totalpage
          })
        }
        if (alldata.length) {
          this.setState({
            nodata: true,
            result: alldata,
            loading: false,
            totalpage: totalpage,
            tableHeight: (alldata.length * this.tableliHeight - 15)
          })
        } else {
          this.setState({
            nodata: false,
            result: alldata,
            loading: false,
            totalpage: totalpage,
            tableHeight: 15
          })
        }
      }
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
  changeTable(event) {
    const current = event.detail.current;
    this.handleType(this.navList[current])
  }

  render () {
    const { resultAll, resultChain, resultJoin, type, nodata, tableHeight, tableIndex} = this.state;
    const { counterStore: { currentcity } } = this.props
    // console.log(tableHeight)
    return (
      <View className="page door-page">
        <Header cityname={currentcity.name} areaname= {currentcity.areaname}></Header>
        <View className="door-page-content">
          <View className="page-title">
            {
              this.navList.map((item, index) => (
                <View
                  className={item.id === type ? 'title title-active' : 'title'}
                  key = {item.id}
                  onClick = {(e) => this.handleType(item,e)}
                >
                  {item.name}
                </View>
              ))
            }
          </View>
          <ScrollView
            className="content-wrapper"
            scrollY
            style={{ height: getWindowHeight(false, 95)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          >
            <Swiper
              className="table-box-swiper"
              current={tableIndex}
              style={{ height: tableHeight + 'px'}}
              onChange = {this.changeTable.bind(this)}
            >
              <SwiperItem>
                <View className="door-table-box">
                  {
                    resultAll.map((item, index) => (
                      <View className="door-table-li" key={item.id}>
                        <View className="door-li-title">
                          <View className="name">{item.merchantname}</View>
                          {
                            item.merchanttype === 1 ?
                            (<View className="type">加盟店</View>) : (<View className="type">直营店</View>)
                          }
                        </View>
                        <Image src={item.merchantimage} className="door-li-image"/>
                        <View className="door-li-content">
                          <View className="li-content">
                            <View className="label">店铺地址：</View>
                            <View className="name">{item.address}</View>
                          </View>
                          <View className="li-content">
                            <View className="label">店铺交通：</View>
                            <View className="name">{item.traffic}</View>
                          </View>
                          <View className="li-content">
                            <View className="label">店铺简介：</View>
                            <View className="name">{item.introduction}</View>
                          </View>
                        </View>
                      </View>
                    ))
                  }
                </View>
              </SwiperItem>
              <SwiperItem>
                <View className="door-table-box">
                  {
                    resultChain.map((item, index) => (
                      <View className="door-table-li" key={item.id}>
                        <View className="door-li-title">
                          <View className="name">{item.merchantname}</View>
                          {
                            item.merchanttype === 1 ?
                            (<View className="type">加盟店</View>) : (<View className="type">直营店</View>)
                          }
                        </View>
                        <Image src={item.merchantimage} className="door-li-image"/>
                        <View className="door-li-content">
                          <View className="li-content">
                            <Text className="label">店铺地址：</Text>
                            <Text className="name">{item.address}</Text>
                          </View>
                          <View className="li-content">
                            <Text className="label">店铺交通：</Text>
                            <Text className="name">{item.traffic}</Text>
                          </View>
                          <View className="li-content">
                            <Text className="label">店铺简介：</Text>
                            <Text className="name">{item.introduction}</Text>
                          </View>
                        </View>
                      </View>
                    ))
                  }
                </View>
              </SwiperItem>
              <SwiperItem>
                <View className="door-table-box">
                  {
                    resultJoin.map((item, index) => (
                      <View className="door-table-li" key={item.id}>
                        <View className="door-li-title">
                          <View className="name">{item.merchantname}</View>
                          {
                            item.merchanttype === 1 ?
                            (<View className="type">加盟店</View>) : (<View className="type">直营店</View>)
                          }
                        </View>
                        <Image src={item.merchantimage} className="door-li-image"/>
                        <View className="door-li-content">
                          <View className="li-content">
                            <Text className="label">店铺地址：</Text>
                            <Text className="name">{item.address}</Text>
                          </View>
                          <View className="li-content">
                            <Text className="label">店铺交通：</Text>
                            <Text className="name">{item.traffic}</Text>
                          </View>
                          <View className="li-content">
                            <Text className="label">店铺简介：</Text>
                            <Text className="name">{item.introduction}</Text>
                          </View>
                        </View>
                      </View>
                    ))
                  }
                </View>
              </SwiperItem>
            </Swiper>
            {
              !nodata && <Nodata></Nodata>
            }
            {this.state.loading &&
              <View className='home__loading'>
                <Text className='home__loading-txt'>正在加载中...</Text>
              </View>
            }
            {!this.state.hasMore &&
              <View className='home__loading home__loading--not-more'>
                <Text className='home__loading-txt'>没有更多内容了</Text>
              </View>
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Door
