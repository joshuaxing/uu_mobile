import Taro, { Component } from '@tarojs/taro'
import { View, Text, Picker } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import http from '../../http/main.js'
import { AtToast } from "taro-ui"

@inject('counterStore')
@observer
class Order extends Component {

  config = {
    navigationBarTitleText: '婚庆预约'
  }
  state = {
    areaList: [],
    styleList: [],
    moneyList: [],
    arrangedList: ['是', '否'],
    moneytypeid: '',
    moneytype: '',
    stylename: '',
    styleid: '',
    arranged: '',
    weddingdate: '',
    areaname: '',
    areaid: ''
  }
  componentWillMount () { 
    this.gotStyle();
    this.gotMoney();
    this.gotArea();
  }

  componentWillReact () {
    
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  gotArea () {
    const { currentcity } = this.props.counterStore
    http('/areas/selectbyparentid.do', {
      parentid: currentcity.id
    })
    .then((result) => {
      if (result.status === 1) {
        let areaList = result.data;
        this.setState({
          areaList: areaList
        })
      }
    })
  }
  gotStyle () {
    http('/app/productcharacteristicstype.do')
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          styleList: result.data
        })
      }
    })
  }
  gotMoney() {
    http('/app/moneytype.do', {
      type: 6
    })
      .then((result) => {
        if (result.status === 1) {
          this.setState({
            moneyList: result.data
          })
        }
      })
  }
  onDateChange = e => {
    this.setState({
      weddingdate: e.detail.value
    })
  }
  onStyleChange = e => {
    const index = e.detail.value;
    const { styleList } = this.state
    this.setState({
      styleid: styleList[index].id,
      stylename: styleList[index].name
    })
  }
  onMoneyChange = e => {
    const index = e.detail.value;
    const { moneyList } = this.state
    this.setState({
      moneytypeid: moneyList[index].id,
      moneytype: moneyList[index].name
    })
  }
  onAreaChange = e => {
    const index = e.detail.value;
    const { areaList } = this.state
    this.setState({
      areaname: areaList[index].areaname,
      areaid: areaList[index].areaid
    })
  }
  onArrangedChange = e => {
    const index = e.detail.value;
    const { arrangedList } = this.state
    this.setState({
      arranged: arrangedList[index]
    })
  }
  resetProductSubmit () {
    this.setState({
      moneytypeid: '',
      moneytype: '',
      stylename: '',
      styleid: '',
      arranged: '',
      weddingdate: '',
      areaname: '',
      areaid: ''
    })
  }
  productSubmit () {
    const {moneytype, stylename, arranged, areaname, weddingdate} = this.state;
    const { user } = this.props.counterStore;
    if (!user.id) {
      Taro.navigateTo({
        url: '/pages/login/index'
      })
      return
    } else if (!weddingdate) {
      Taro.showToast({
        title: '请选择婚礼日期',
        icon: 'none',
        duration: 2000
      })
      return
    } else if (!stylename) {
      Taro.showToast({
        title: '请选择婚礼风格',
        icon: 'none',
        duration: 2000
      })
      return
    } else if (!moneytype) {
      Taro.showToast({
        title: '请选择婚礼预算',
        icon: 'none',
        duration: 2000
      })
      return
    } else if (!arranged) {
      Taro.showToast({
        title: '请选择是否一站式包办',
        icon: 'none',
        duration: 2000
      })
      return
    } else if (!areaname) {
      Taro.showToast({
        title: '请选择预约区域',
        icon: 'none',
        duration: 2000
      })
      return
    }
    this.productPost()
  }
  productPost() {
    const { currentcity, user } = this.props.counterStore
    http("/app/addmerchantsubscribe.do", {
      merchantid: 0,
      userid: user.id,
      phone: user.account,
      moneytype: this.state.moneytype,
      stylename: this.state.stylename,
      arranged: this.state.arranged,
      weddingdate: this.state.weddingdate,
      areaname: this.state.areaname,
      cityname: currentcity.name
    }).then((result) => {
      if (result.status === 1) {
        this.resetProductSubmit();
      }
      Taro.showToast({
        title: result.msg,
        icon: 'none',
        duration: 2000
      })
    })
  }
  render () {
    const {
      styleList,
      stylename,
      weddingdate,
      arrangedList,
      arranged,
      areaList,
      areaname,
      moneyList,
      moneytype
    } = this.state;
    return (
      <View className="page order-page">
        <View className="title">优品婚匠 专属您的一站式婚礼</View>
        <View className="form-items">
          <View className="label">选择婚期:</View>
          <View className="form-item">
            <Picker mode='date' onChange={this.onDateChange}>
              <View className='picker'>
                {weddingdate}
              </View>
            </Picker>
          </View>
        </View>
        <View className="form-items">
          <View className="label">婚礼风格:</View>
          <View className="form-item">
            <Picker mode='selector' range={styleList} onChange={this.onStyleChange} rangeKey='name'>
              <View className='picker'>
                {stylename}
              </View>
            </Picker>
          </View>
        </View>
        <View className="form-items">
          <View className="label">婚礼预算:</View>
          <View className="form-item">
            <Picker mode='selector' range={moneyList} onChange={this.onMoneyChange} rangeKey='name'>
              <View className='picker'>
                {moneytype}
              </View>
            </Picker>
          </View>
        </View>
        <View className="form-items">
          <View className="label">预约区域:</View>
          <View className="form-item">
            <Picker mode='selector' range={areaList} onChange={this.onAreaChange} rangeKey='areaname'>
              <View className='picker'>
                {areaname}
              </View>
            </Picker>
          </View>
        </View>
        <View className="form-items">
          <View className="label">一站式包办:</View>
          <View className="form-item">
            <Picker mode='selector' range={arrangedList} onChange={this.onArrangedChange}>
              <View className='picker'>
                {arranged}
              </View>
            </Picker>
          </View>
        </View>
        <View className="submit-btn" onClick={this.productSubmit.bind(this)}>立即预约-婚礼管家</View>
      </View>
    )
  }
}

export default Order
