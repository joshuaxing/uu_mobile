/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-29 09:32:33
 * @LastEditTime: 2019-08-20 17:20:13
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import addressIcon from './address_icon.png'
import trafficIcon from './traffic_icon.png'
import arrowRight from './arrows_right.png'
import arrowDown from './arrows_down.png'
import http from '../../http/main.js'
import Banner from './banner/index.js'
@inject('counterStore')
@observer
class HotelDetail extends Component {

  config = {
    navigationBarTitleText: '酒店详情'
  }
  state = {
    result: {},
    detailid: 0,
    bannerlist: [],
    typelist: [],
    navIndex: 0,
    hallData: [],
    menuData: [{
      isdown: false
    }]
  }
  componentWillMount () {
    const id = this.$router.params.id;
    // console.log(id)
    this.setState({
      detailid: id
    }, () => {
      this.gotData()
    })
  }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () {

  }

  componentDidShow () { }

  componentDidHide () { }
  gotData () {
    http('/app/hotelinfo.do', {
      id: this.state.detailid
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data;
        const value = {
          ...data,
          titleimage: data.pcpath[0],
          menunum: data.menudata.length,
          banquethallnum: data.banquethalldata.length
        }
        let menuData = data.menudata;
        menuData.map((item, index) => {
          item.isdown = false
        })
        this.setState({
          result: value,
          bannerlist: data.pcpath,
          typelist: data.hotelcharacteristicmsg,
          hallData: data.banquethalldata,
          menuData: menuData
        })
      }
    })
  }
  handleClick (index) {
    this.setState({
      navIndex: index
    })
  }
  clickDown (value) {
    let menuData = this.state.menuData;
    menuData.map((item, index) => {
      if (value.id === item.id) {
        item.isdown = !value.isdown;
      }
    })
    this.setState({
      menuData: menuData
    })
  }
  goMap () {
    const { result } = this.state;
    const vaule = {
      hotelname: result.hotelname,
      latitude: result.latitude,
      longitude: result.longitude
    }
    Taro.navigateTo({
      url: `/pages/map/index?id=${JSON.stringify(vaule)}`
    })
  }
  prevImage (item) {
    Taro.previewImage({
      current: item.pcpath[0],
      urls: item.pcpath
    })
  }
  render () {
    const { result, bannerlist, typelist, navIndex, hallData, menuData} = this.state;
    return (
      <View className="page hoteldetail-page">
        <View className="top-banner">
          <Banner list={bannerlist}></Banner>
        </View>
        <View className="header">
          <View className="name-price">
            <View className="name">{result.hotelname}</View>
            <View className="price">{result.moneynum}</View>
          </View>
          <View className="info">
            <Text className="info-type">{result.typename}</Text>
            <Text className="info-line">|</Text>
            <Text className="info-table">{result.tablenum}</Text>
            <Text className="info-line">|</Text>
            <Text className="info-table">{result.regionname}</Text>
          </View>
          <View className="label">
            {
              typelist.map((item, index) => (
                <Text className="label-txt" key={index}>{item}</Text>
              ))
            }
          </View>
        </View>
        <View className="white-space"></View>
        <View className="hotel-content">
          <View className="content-title">
            <View
              className = {navIndex === 0 ? 'title title-active' : 'title'}
              onClick={this.handleClick.bind(this, 0)}
            >
              <View className="text">详情</View>
            </View>
            <View
              className = {navIndex === 1 ? 'title title-active' : 'title'}
              onClick={this.handleClick.bind(this, 1)}
            >
              <View className="text">宴会厅({hallData.length})</View>
            </View>
            <View
              className = {navIndex === 2 ? 'title title-active' : 'title'}
              onClick={this.handleClick.bind(this, 2)}
            >
              <View className="text">菜单({menuData.length})</View>
            </View>
          </View>

          <View className="content-ul">
            {
              navIndex === 0 &&
              (<View className="content-li">
                  <Image src={result.titleimage} className="titleimage"/>
                  <View className="hotel-des">
                    <View className="des-title">酒店介绍</View>
                    <View className="des-content">{result.introduction}</View>
                  </View>
                  {
                    process.env.TARO_ENV === 'weapp' ?
                    (
                      <View className="hotel-address" onClick={this.goMap.bind(this)}>
                        <Image src={addressIcon} className="icon"/>
                        <View className="text">{result.address}</View>
                        <Image src={arrowRight} className="icon"/>
                      </View>
                    ) : null
                  }
                  {
                    process.env.TARO_ENV === 'h5' ?
                    (
                      <View className="hotel-address">
                        <Image src={addressIcon} className="icon"/>
                        <View className="text">{result.address}</View>
                      </View>
                    ) : null
                  }

                  <View className="hotel-traffic">
                    <Image src={trafficIcon} className="icon"/>
                    <View className="text">{result.traffic}</View>
                  </View>
              </View>)
            }

            {
              navIndex === 1 &&
              (<View className="hall-box">
                  {
                    hallData.map((item, index) => (
                      <View className="hall-item" key={item.id}>
                        <View className="hall-image">
                          <Image src={item.pcpath[0]} className="image" onClick={this.prevImage.bind(this, item)}/>
                          <View className="num">可容纳{item.tablenum}桌</View>
                          <View className="prev-tips">预览图片</View>
                        </View>
                        <View className="hall-name">
                          {item.name}
                        </View>
                        <View className="hall-info">
                          <Text className="txt">层高：{item.storeyheight}m</Text>
                          <Text className="txt">面积：{item.area}㎡</Text>
                          <Text className="txt">形状：{item.shape}</Text>
                        </View>
                      </View>
                    ))
                  }
                </View>
              )
            }

            {
              navIndex === 2 &&
              (<View className="menu-box">
                  {
                    menuData.map((item, index) => (
                      <View className="menu-item" key={item.id}>
                        <View className="menu-item-title" onClick={this.clickDown.bind(this,item)}>
                          <Image src={result.titleimage} className="menu-image"/>
                          <View className="menu-info">
                            <View className="name">{item.name}</View>
                            <View className="pice">
                              <Text className="discount">￥{item.discountmoney}</Text>
                              <Text className="old">原价:￥{item.money}</Text>
                            </View>
                          </View>
                          <Image src={arrowDown} className={item.isdown ? 'right-arrow-acitve right-arrow' : 'right-arrow'}/>
                        </View>
                        {
                          item.isdown ? (
                            <View className="menu-item-content">
                              {
                                item.itemlist.map((value, index) => (
                                  <View className="menu-name" key={value.id}>{value.name}</View>
                                ))
                              }
                            </View>
                          ) : null
                        }
                      </View>
                    ))
                  }
                </View>
              )
            }
          </View>
        </View>
      </View>
    )
  }
}

export default HotelDetail
