/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-18 15:23:38
 * @LastEditTime: 2019-08-09 15:07:18
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import Header from '../../components/header/index.js'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'
import phoneIcon from './phone.png'
@inject('counterStore')
@observer
class Hotel extends Component {

  config = {
    navigationBarTitleText: '婚宴酒店',
    enablePullDownRefresh: true,
    onReachBottomDistance: 0
  }
  sortList = [
    {id: 0, name: '默认排序', prosort: '', sorttype: 0},
    {id: 1, name: '价格升序', prosort: 'moneytype', sorttype: 1},
    {id: 2, name: '价格降序', prosort: 'moneytype', sorttype: 2},
    {id: 3, name: '星级升序', prosort: 'type', sorttype: 1},
    {id: 4, name: '星级降序', prosort: 'type', sorttype: 2},
  ]
  state = {
    totalpage: 0,
    page: 1,
    limit: 10,
    nodata: true,
    loading: false,
    hasMore: true,
    result: [],
    selectIndex: 0,
    selectOpen: false,
    sortform: {
      id: 0,
      prosort: '',
      sorttype: 0,
      name: '默认排序'
    },
    typeform: {
      id: 0,
      name: '全部酒店'
    },
    otherform: {
      maxtable: 0,
      moneytype: 0,
      characteristic: 0
    },
    typeList: [],
    tableList: [],
    pirceList: [],
    styleList: []
  }
  componentWillMount () { 
    this.gotData();
    this.gotSelectData();
  }

  componentWillReact () {
    this.gotSelectData();
    this.setState({
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  componentDidMount () { 
    
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  onPullDownRefresh() {
    //下拉刷新
    this.gotSelectData();
    this.setState({
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  selectMoney(item, e) {
    let otherform = this.state.otherform;
    otherform.moneytype = item.id
    this.setState({
      otherform: otherform
    })
    e.stopPropagation();
  }
  selectType(item, e) {
    let otherform = this.state.otherform;
    otherform.characteristic = item.id
    this.setState({
      otherform: otherform
    })
    e.stopPropagation();
  }
  selectTable(item, e) {
    let otherform = this.state.otherform;
    otherform.maxtable = item.id
    this.setState({
      otherform: otherform
    })
    e.stopPropagation();
  }
  sureOther () {
    this.setState({
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }
  resetOther (e) {
    this.setState({
      otherform: {
        maxtable: 0,
        moneytype: 0,
        characteristic: 0
      }
    })
    e.stopPropagation();
  }
  searchSort (item) {
    this.setState({
      sortform: item,
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }
  searchType(item) {
    this.setState({
      typeform: item,
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
    
  }
  colseSelect () {
    this.setState({
      selectIndex: 0,
      selectOpen: false
    })
  }
  handleSort(index) {
    let selectOpen = this.state.selectOpen;
    let selectIndex = this.state.selectIndex;
    if (selectIndex !== index && selectOpen) {
      this.setState({
        selectIndex: index
      })
    } else {
      this.setState({
        selectIndex: index,
        selectOpen: !selectOpen
      })
    }
  }
  gotSelectData () {
    //酒店类型数据
    http('/app/hotletype.do')
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "全部酒店"
        })
        this.setState({
          typeList: data
        })
      }
    })
    //酒店桌数
    http('/app/tabletype.do')
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "不限"
        })
        this.setState({
          tableList: data
        })
      }
    })
    //酒店金额
    http('/app/moneytype.do',{
      type: 1
    })
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "不限"
        })
        this.setState({
          pirceList: data
        })
      }
    })
    //酒店特色
    http('/app/styletype.do',{
      type: 3
    })
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "不限"
        })
        this.setState({
          styleList: data
        })
      }
    })
  }
  gotData () {
    //酒店列表数据
    const { counterStore: { currentcity } } = this.props
    const { page, limit, hasMore, loading, otherform, typeform, sortform} = this.state;
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true })
    http('/app/hotellistbycity.do', {
      cityid: currentcity.id,
      areaid: currentcity.areaid,
      page: page,
      limit: limit,
      type: typeform.id,
      maxtable: otherform.maxtable,
      moneytype: otherform.moneytype,
      characteristic: otherform.characteristic,
      prosort: sortform.prosort,
      sorttype: sortform.sorttype
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (alldata.length) {
           this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: true
           })
        } else {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: false
          })
        }
      }
      Taro.stopPullDownRefresh();
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
  handleDetail (item) {
    Taro.navigateTo({
      url: `/pages/hoteldetail/index?id=${item.id}`
    })
  }
  orderHotel (item) {
    const { counterStore: { user } } = this.props
    if (!user.id) {
      Taro.navigateTo({
        url: '/pages/login/index'
      })
    } else {
      http('/app/addhotelsubscribe.do', {
        hotelid: item.id,
        userid: user.id,
        phone: user.account
      })
      .then((result) => {
        Taro.showToast({
          title: result.msg,
          icon: 'none',
          duration: 2000
        })
      })
    }
  }
  render () {
    const {
      result,
      sortform,
      typeform,
      typeList, 
      selectIndex,
      selectOpen,
      nodata,
      pirceList, 
      styleList, 
      tableList, 
      otherform
    } = this.state;
    const { counterStore: { currentcity } } = this.props;
    let searchshow = 0;
    if (selectIndex === 1 && selectOpen) {
      searchshow = 1;
    } else if (selectIndex === 2 && selectOpen) {
      searchshow = 2;
    } else if (selectIndex === 3 && selectOpen) {
      searchshow = 3;
    } else {
      searchshow = 0;
    }
    return (
      <View className="page hotel-page">
        <Header cityname={currentcity.name} areaname= {currentcity.areaname}></Header>
        <View className="hotel-page-content">
          <View className="search-result-box">
            <View className="search-title-ul">
              <View
                onClick={this.handleSort.bind(this, 1)}
                className={selectIndex === 1 && selectOpen ? 'search-title search-title-acitve' : 'search-title'}
              >
                <Text className="title-txt">{sortform.name}</Text>
                <View className="title-icon at-icon at-icon-chevron-down"></View>
              </View>
              <View
                onClick={this.handleSort.bind(this, 2)}
                className={selectIndex === 2 && selectOpen ? 'search-title search-title-acitve' : 'search-title'}
              >
                <View className="title-txt">{typeform.name}</View>
                <View className="title-icon at-icon at-icon-chevron-down"></View>
              </View>
              <View
                className={selectIndex === 3 && selectOpen ? 'search-title search-title-acitve' : 'search-title'}
                onClick={this.handleSort.bind(this, 3)}
              >
                <View className="title-txt">智能筛选</View>
                <View className="title-icon at-icon at-icon-chevron-down"></View>
              </View>
            </View>
            {/* 筛选条件 */}

            {/* 默认排序 */}
            {
              searchshow === 1 &&
              (<View className="search-content-ul" onClick={this.colseSelect.bind(this)}>
                <View className="search-content-sort">
                  {
                    this.sortList.map((item) => (
                      <View
                        key={item.id}
                        className={item.id === sortform.id ? 'sort-li sort-li-acitve' : 'sort-li'}
                        onClick={this.searchSort.bind(this, item)}
                      >
                        <View className="sort-txt">{item.name}</View>
                        <View className="sort-txt-icon at-icon at-icon-check"></View>
                      </View>
                    ))
                  }
                </View>
              </View>)
            }
            {/* 类型排序 */}
            {
              searchshow === 2 &&
              (<View className="search-content-ul" onClick={this.colseSelect.bind(this)}>
                  <View className="search-content-sort">
                    {
                      typeList.map((item) => (
                        <View
                          key={item.id}
                          className={item.id === typeform.id ? 'sort-li sort-li-acitve' : 'sort-li'}
                          onClick={this.searchType.bind(this, item)}
                        >
                          <View className="sort-txt">{item.name}</View>
                          <View className="sort-txt-icon at-icon at-icon-check"></View>
                        </View>
                      ))
                    }
                  </View>
                </View>
              )
            }
            {/* 智能排序 */}
            {
              searchshow === 3 &&
                (<View className="search-content-ul" onClick={this.colseSelect.bind(this)}>
                  <View className="search-content-other" onClick={(e) => e.stopPropagation()}>
                    <View className="content-other-box">
                      <View className="other-item-ul">
                        <View className="other-item-title">价格</View>
                        <View className="other-item-li">
                          {
                            pirceList.map((item) => (
                              <View
                                key = {item.id}
                                className={otherform.moneytype === item.id ? 'other-item other-item-active' : 'other-item'}
                                onClick={this.selectMoney.bind(this, item)}
                              >{item.name}</View>
                            ))
                          }
                        </View>
                      </View>
                      <View className="other-item-ul">
                        <View className="other-item-title">特色</View>
                        <View className="other-item-li">
                          {
                            styleList.map((item) => (
                              <View
                                key={item.id}
                                className={otherform.characteristic === item.id ? 'other-item other-item-active' : 'other-item'}
                                onClick={this.selectType.bind(this, item)}
                              >{item.name}</View>
                            ))
                          }
                        </View>
                      </View>
                      <View className="other-item-ul">
                        <View className="other-item-title">桌数</View>
                        <View className="other-item-li">
                          {
                            tableList.map((item) => (
                              <View
                                key={item.id}
                                className={otherform.maxtable === item.id ? 'other-item other-item-active' : 'other-item'}
                                onClick={this.selectTable.bind(this, item)}
                              >{item.name}</View>
                            ))
                          }
                        </View>
                      </View>
                    </View>
                    <View className="operation-btns">
                      <View className="cancle-btn" onClick={this.resetOther.bind(this)}>重置</View>
                      <View className="sure-btn" onClick={this.sureOther.bind(this)}>确定</View>
                    </View>
                  </View>
                </View>)
            }
          </View>
          <ScrollView 
            className="content-wrapper"
            scrollY
            style={{ height: getWindowHeight(true, 80)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          >
            <View className="content-item-ul">
              {
                result.map((item) => (
                  <View className="content-item-li" key={item.id}>
                    <View className="content-item-image" onClick={this.handleDetail.bind(this, item)}>
                      <Image src={item.pcpath[0]}  className="item-image"></Image>
                    </View>
                    <View className="content-item" onClick={this.handleDetail.bind(this, item)}>
                      <View className="name">{item.hotelname}</View>
                      <View className="price">{item.moneynum}</View>
                      <View className="table">
                        <Text>可容纳{item.tablenum}</Text>
                        <Text className="line">|</Text>
                        <Text>{item.typename}</Text>
                      </View>
                      <View className="lable">{item.address}</View>
                    </View>
                    <View className="content-item-icon" onClick={this.orderHotel.bind(this,item)}>
                      <View className="icon-box">
                        <Image src={phoneIcon} className="phone-icon"/>
                        <View className="btn">立即预约</View>
                      </View>
                    </View>
                  </View>
                ))
              }
            </View>
            {
              !nodata && <Nodata></Nodata>
            }
            {this.state.loading &&
              <View className='home__loading'>
                <Text className='home__loading-txt'>正在加载中...</Text>
              </View>
            }
            {!this.state.hasMore &&
              <View className='home__loading home__loading--not-more'>
                <Text className='home__loading-txt'>没有更多内容了</Text>
              </View>
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Hotel
