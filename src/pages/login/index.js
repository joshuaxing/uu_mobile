import Taro, { Component } from '@tarojs/taro'
import { View, Image, Input} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

import logo from './uu_logo_black.png'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class Login extends Component {

  codeTimer = null
  config = {
    navigationBarTitleText: '注册登录'
  }
  state = {
    phone: '',
    code: '',
    lasttime: false,
    ischeck: false,
    disabledCode: true,
    codetext: '获取验证码',
    timenum: 60
  }
  
  componentWillMount () { }

  componentWillReact () {
    //console.log('componentWillReact')
  }

  componentDidMount () { }

  componentWillUnmount () { 
    clearInterval(this.codeTimer)
  }

  componentDidShow () { }

  componentDidHide () { }
  handlePhoneInput = (e) => {
    // console.log(e.detail.value)
    const value = e.detail.value;
    if (!/^1[3456789]\d{9}$/.test(value)) {
      this.setState({
        ischeck: false,
        phone: value,
        disabledCode: true
      })
    } else {
      //手机号正确
      if (!this.state.lasttime) {
        this.setState({
          disabledCode: false,
          ischeck: true,
          phone: value
        })
      } else {
        //倒计时中
        this.setState({
          disabledCode: true,
          ischeck: true,
          phone: value
        })
      }
    }
    
  }
  handleCodeInput = (e) => {
    const value = e.detail.value;
    this.setState({
      code: value
    })
  }
  submitCode = () => {
    const {phone, disabledCode} = this.state;
    if (disabledCode) {
      return
    }
    //获取验证码
    http('/app/sendsms.do', {
      account: phone
    })
    .then((result) => {
      if (result.status === 1) {
        //倒计时
        this.countTime();
      }
      Taro.showToast({
        title: result.msg,
        icon: 'none',
        duration: 2000
      })
    })
    
  }
  countTime () {
    this.setState({
      codetext: this.state.timenum + 's',
      disabledCode: true,
      lasttime: true
    }, () => {
      this.codeTimer = setInterval(() => {
        this.setState((state, props) => ({
          timenum: state.timenum - 1,
          codetext: state.timenum + 's'
        }))
        if (this.state.timenum <= 0) {
          this.resetTime()
        }
      }, 1000)
    })
  }
  resetTime () {
    clearInterval(this.codeTimer)
    if (this.state.ischeck) {
      // console.log('手机号正确')
      this.setState({
        disabledCode: false,
        codetext: '获取验证码',
        timenum: 60,
        lasttime: false
      })
    } else {
      // console.log('手机号错误')
      this.setState({
        disabledCode: true,
        codetext: '获取验证码',
        timenum: 60,
        lasttime: false
      })
    }
  }
  submitLogin () {
    const { phone, code} = this.state;
    const { counterStore } = this.props;
    if (!/^1[3456789]\d{9}$/.test(phone)) {
      Taro.showToast({
        title: '手机号不合法',
        icon: 'none',
        duration: 2000
      })
    } else if (!code) {
      Taro.showToast({
        title: '请输入验证码',
        icon: 'none',
        duration: 2000
      })
    } else {
      http('/app/login.do', {
        account: phone,
        code: code
      })
      .then((result) => {
        if (result.status === 1) {
          const value = {
            account: result.data.account,
            id: result.data.id
          }
          counterStore.gotUserInfo(value)
          Taro.setStorageSync('uu_user2030', JSON.stringify(value))
          Taro.navigateBack({
            delta: 1
          })
        } else {
          Taro.showToast({
            title: result.msg,
            icon: 'none',
            duration: 2000
          })
        }
      })
    }
  }
  render () {
    const { phone, code, disabledCode, codetext} = this.state;
    return (
      <View className="page login-page">
        <View className="space"></View>
        <Image src = {logo} className="logo-image"/>
        <View className="login-title">登录/注册</View>
        <View className="login-form">
          <View className="form-item">
            <View className="input-width">
              <Input type="number" placeholder="请输入手机号" 
                className="input-el" 
                onInput={this.handlePhoneInput}
                value={phone}
                maxLength = {11}
              />
            </View>
          </View>
          <View className="form-item">
            <View className="input-width">
              <Input type="number" placeholder="请输入验证码" className="input-el" value={code} maxLength = {6} onInput={this.handleCodeInput}/>
            </View>
            <View className={disabledCode ? 'code-btn' : 'code-btn code-btn-disable'} onClick={this.submitCode}>{codetext}</View>
          </View>
        </View>
        <View className="submit-btn" onClick={this.submitLogin.bind(this)}>登 录</View>
      </View>
    )
  }
}

export default Login
