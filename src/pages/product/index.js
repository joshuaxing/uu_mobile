/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-23 13:25:07
 * @LastEditTime: 2019-08-09 15:08:44
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, Swiper, SwiperItem} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import Header from '../../components/header/index.js'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class Product extends Component {
  tableLiHeight = 317
  config = {
    navigationBarTitleText: '婚庆产品',
    enablePullDownRefresh: true,
    onReachBottomDistance: 0
  }
  navList = [
    {id: 0, name: '全部'},
    {id: 1, name: '最新'},
    {id: 2, name: '最热'},
  ]
  state = {
    totalpage: 0,
    alltotalpage: 0,
    newtotalpage: 0,
    hottotalpage: 0,
    page: 1,
    limit: 8,
    type: 0,
    nodata: true,
    loading: false,
    hasMore: true,
    result: [],
    resultAll: [],
    resultNew: [],
    resultHot: [],
    tableHeight: 150,
    tableIndex: 0
  }

  componentWillMount () { 
    this.gotData();
  }

  componentWillReact () {
    this.setState({
      result: [],
      resultAll: [],
      resultNew: [],
      resultHot: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
    console.log('componentWillReact')
  }

  componentDidMount () {
    /*
    setTimeout(() => {
      const query = Taro.createSelectorQuery();
      query.select('#table-one').boundingClientRect((react) => {
        console.log(react.height)
        this.setState({
          tableHeight: react.height
        })
      }).exec()
    }, 16)
    */
  }

  componentWillUnmount () { }

  componentDidShow () { 

  }

  componentDidHide () { }


  onPullDownRefresh () {
    this.setState({
      result: [],
      resultAll: [],
      resultNew: [],
      resultHot: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  handleType (item) {
    const { resultAll, resultNew, resultHot, alltotalpage, newtotalpage, hottotalpage} = this.state;
    if (item.id === 0) {
      !resultAll.length ? 
        this.setState({
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          tableIndex: item.id,
          type: item.id
        }, () => {
          this.gotData();
        }) : 
        this.setState({
          result: resultAll,
          tableIndex: item.id,
          type: item.id,
          tableHeight: (resultAll.length * this.tableLiHeight - 15),
          totalpage: alltotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    } else if (item.id === 1) {
      !resultNew.length ?
        this.setState({
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          tableIndex: item.id,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          result: resultNew,
          tableIndex: item.id,
          type: item.id,
          tableHeight: (resultNew.length * this.tableLiHeight - 15),
          totalpage: newtotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    } else {
      !resultHot.length ?
        this.setState({
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          tableIndex: item.id,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          result: resultHot,
          tableIndex: item.id,
          type: item.id,
          tableHeight: (resultHot.length * this.tableLiHeight - 15),
          totalpage: hottotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    }
  }
  gotData () {
    //列表数据
    const { counterStore: { currentcity } } = this.props
    const { page, limit, hasMore, loading, type} = this.state;
    
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true})
    http('/app/productlistbycity.do', {
      cityid: currentcity.id,
      areaid: currentcity.areaid,
      page: page,
      limit: limit,
      type: type
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (type === 0) {
          this.setState({
            resultAll: alldata,
            alltotalpage: totalpage
          })
        } else if (type === 1) {
          this.setState({
            resultNew: alldata,
            newtotalpage: totalpage
          })
        } else {
          this.setState({
            resultHot: alldata,
            hottotalpage: totalpage
          })
        }
        if (alldata.length) {
          this.setState({
            nodata: true,
            result: alldata,
            loading: false,
            totalpage: totalpage,
            tableHeight: (alldata.length * this.tableLiHeight - 15)
          })
        } else {
          this.setState({
            nodata: false,
            result: alldata,
            loading: false,
            totalpage: totalpage,
            tableHeight: 15
          })
        }
        Taro.stopPullDownRefresh()
      }
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
 
  changeTable(event) {
    const current = event.detail.current;
    this.handleType(this.navList[current])
  }
  handleDetail(item) {
     Taro.navigateTo({
       url: `/pages/productdetail/index?id=${item.id}`
     })
  }
  render () {
    const { resultAll, resultHot, resultNew, type, nodata, tableHeight, tableIndex} = this.state;
    const { counterStore: { currentcity } } = this.props;
    return (
      <View className="page product-page">
        <Header cityname={currentcity.name} areaname= {currentcity.areaname}></Header>
        <View className="product-page-content">
          <View className="page-title">
            {
              this.navList.map((item, index) => (
                <View 
                  className={item.id === type ? 'title title-active' : 'title'}
                  key = {item.id}
                  onClick = {this.handleType.bind(this,item)}
                >
                  {item.name}
                </View>
              ))
            }
          </View>
          <ScrollView 
            className="content-wrapper"
            scrollY
            style={{ height: getWindowHeight(true, 95)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          > 
            <Swiper 
              className="table-box-swiper" 
              current={tableIndex}
              style={{ height: tableHeight + 'px'}}
              onChange = {this.changeTable.bind(this)}
            >
              <SwiperItem>
                <View className="table-box" id="table-one">
                  {
                    resultAll.map((item, index) => (
                      <View className="table-li" key={item.id} onClick={this.handleDetail.bind(this, item)}>
                        <Image src={item.summarypc[0]} className="li-image"/>
                        <View className="li-content">
                          <View className="name">
                            <View className="name-title">{item.name}</View>
                            {
                              item.labelid === 1 &&
                              (<View className="name-lable">最新</View>) 
                            }
                            {
                              item.labelid === 2 &&
                              (<View className="name-lable">最热</View>)
                            }
                          </View>
                          <View className="price">
                            <View className="discount">￥<Text className="price-txt">{item.discountmoney}</Text></View>
                            <View className="old">原价:￥{item.money}</View>
                          </View>
                          <View className="lable">
                            <View className="lable-name">{item.characteristicname}</View>
                          </View>
                          <View className="des">{item.introduction}</View>
                        </View>
                      </View>
                    ))
                  }
                  
                </View>
              </SwiperItem>
              <SwiperItem>
                <View className="table-box" id="table-two">
                  {
                    resultNew.map((item, index) => (
                      <View className="table-li" key={item.id} onClick={this.handleDetail.bind(this, item)}>
                        <Image src={item.summarypc[0]} className="li-image"/>
                        <View className="li-content">
                          <View className="name">
                            <View className="name-title">{item.name}</View>
                            {
                              item.labelid === 1 &&
                              (<View className="name-lable">最新</View>) 
                            }
                            {
                              item.labelid === 2 &&
                              (<View className="name-lable">最热</View>)
                            }
                          </View>
                          <View className="price">
                            <View className="discount">￥<Text className="price-txt">{item.discountmoney}</Text></View>
                            <View className="old">原价:￥{item.money}</View>
                          </View>
                          <View className="lable">
                            <View className="lable-name">{item.characteristicname}</View>
                          </View>
                          <View className="des">{item.introduction}</View>
                        </View>
                      </View>
                    ))
                  }
                  
                </View>
              </SwiperItem>
              <SwiperItem>
                <View className="table-box" id="table-three">
                  {
                    resultHot.map((item, index) => (
                      <View className="table-li" key={item.id} onClick={this.handleDetail.bind(this, item)}>
                        <Image src={item.summarypc[0]} className="li-image"/>
                        <View className="li-content">
                          <View className="name">
                            <View className="name-title">{item.name}</View>
                            {
                              item.labelid === 1 &&
                              (<View className="name-lable">最新</View>) 
                            }
                            {
                              item.labelid === 2 &&
                              (<View className="name-lable">最热</View>)
                            }
                          </View>
                          <View className="price">
                            <View className="discount">￥<Text className="price-txt">{item.discountmoney}</Text></View>
                            <View className="old">原价:￥{item.money}</View>
                          </View>
                          <View className="lable">
                            <View className="lable-name">{item.characteristicname}</View>
                          </View>
                          <View className="des">{item.introduction}</View>
                        </View>
                      </View>
                    ))
                  }
                  
                </View>
              </SwiperItem>
            </Swiper>
            {
              !nodata && <Nodata></Nodata>
            }
            {this.state.loading &&
              <View className='home__loading'>
                <Text className='home__loading-txt'>正在加载中...</Text>
              </View>
            }
            {!this.state.hasMore &&
              <View className='home__loading home__loading--not-more'>
                <Text className='home__loading-txt'>没有更多内容了</Text>
              </View>
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Product
