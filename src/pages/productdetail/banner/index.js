/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-29 15:41:02
 * @LastEditTime: 2019-08-20 17:30:23
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('counterStore')
@observer
class SwiperBanner extends Component {

  static defaultProps = {
    list: []
  }
  componentWillMount () { }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  prevImage (item) {
    const { list } = this.props
    Taro.previewImage({
      current: item,
      urls: list
    })
  }

  render () {
    const { list } = this.props
    return (
      <View className='productdetail-banner'>
        <Swiper
          className='productdetail-banner__swiper'
          circular
          autoplay
          indicatorDots = {true}
          indicatorColor = 'rgb(255,255,255,1)'
          indicatorActiveColor='rgb(252,71,71)'
          circular = {true}
          // TODO 目前 H5、RN 暂不支持 previousMargin、nextMargin
          // previousMargin
          // nextMargin
        >
          {list.map((item, index)=> (
            <SwiperItem
              key={index}
              className='productdetail-banner__swiper-item'
            >
              <Image
                className='productdetail-banner__swiper-item-img'
                src={item}
                onClick={this.prevImage.bind(this, item)}
              />
            </SwiperItem>
          ))}
        </Swiper>
      </View>
    )
  }
}

export default SwiperBanner
