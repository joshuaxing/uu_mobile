/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-29 15:41:02
 * @LastEditTime: 2019-08-20 17:38:50
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text, Video, ScrollView} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import Banner from './banner/index.js'
import './index.scss'
import closeicon from './close.png'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class ProductDetail extends Component {

  config = {
    navigationBarTitleText: '产品详情'
  }
  state = {
    result: {},
    detailid: 0,
    bannerlist: [],
    stagepc: [],
    ceremonypc: [],
    signpc: [],
    guestspc: [],
    lamppc: [],
    brightspotpc: [],
    levelapc: [],
    levelbpc: [],
    levelcpc: [],
    navIndex: 0,
    tipsValue: 0,
    demomv: '',
    showvideo: false,
    demotitle:  ''
  }
  componentWillMount () {
    const id = this.$router.params.id;
    this.setState({
      detailid: id
    }, () => {
      this.gotData()
    })
  }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () {

  }

  componentDidShow () { }

  componentDidHide () { }
  gotData () {
    http('/app/productinfo.do', {
      id: this.state.detailid
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data;
        let brightspotpcfirst = ''
        if (data.brightspotpc.length) {
          brightspotpcfirst = data.brightspotpc.shift()
        }
        let levelapcfirst = ''
        if (data.levelapc.length) {
          levelapcfirst = data.levelapc[0]
        }
        let levelbpcfirst = ''
        if (data.levelbpc.length) {
          levelbpcfirst = data.levelbpc[0]
        }
        let levelcpcfirst = ''
        if (data.levelcpc.length) {
          levelcpcfirst = data.levelcpc[0]
        }
        let brightspotdes = [];
        if (data.brightspot.length) {
          brightspotdes = data.brightspot.split('|')
        }
        let levelades = [];
        if (data.levela.length) {
          levelades = data.levela.split('|')
        }
        let levelbdes = [];
        if (data.levelb.length) {
          levelbdes = data.levelb.split('|')
        }
        let levelcdes = [];
        if (data.levelc.length) {
          levelcdes = data.levelc.split('|')
        }
        const value = {
          ...data,
          stagepcfirst: data.stagepc[0],
          stagedes: data.stage.split('|'),
          ceremonypcfirst: data.ceremonypc[0],
          ceremonydes: data.ceremony.split('|'),
          signpcfirst: data.signpc[0],
          signdes: data.sign.split('|'),
          guestspcfirst: data.guestspc[0],
          guestsdes: data.guests.split('|'),
          lamppcfirst: data.lamppc[0],
          lampdes: data.lamp.split('|'),
          brightspotpcfirst: brightspotpcfirst,
          brightspotdes: brightspotdes,
          levelapcfirst: levelapcfirst,
          levelades: levelades,
          levelbpcfirst: levelbpcfirst,
          levelbdes: levelbdes,
          levelcpcfirst: levelcpcfirst,
          levelcdes: levelcdes
        }
        let tipsValue = 0
        if (data.levelapc.length || data.levelb.length || data.levelc.length) {
          tipsValue = 1;
        }
        if ((data.levelapc.length && data.levelbpc.length) || (data.levelapc.length && data.levelcpc.length) || (data.levelbpc.length && data.levelcpc.length)) {
          tipsValue = 2;
        }
        if (data.levelapc.length && data.levelbpc.length && data.levelcpc.length) {
          tipsValue = 3;
        }
        this.setState({
          result: value,
          bannerlist: data.summarypc,
          stagepc: data.stagepc,
          ceremonypc: data.ceremonypc,
          signpc: data.signpc,
          guestspc: data.guestspc,
          lamppc: data.lamppc,
          brightspotpc: data.brightspotpc,
          levelapc: data.levelapc,
          levelbpc: data.levelbpc,
          levelcpc: data.levelcpc,
          tipsValue: tipsValue
        })
      }
    })
  }
  handleClick (index) {
    this.setState({
      navIndex: index
    })
  }
  handleVideo (value, title) {
    this.setState({
      showvideo: true,
      demomv: value,
      demotitle: title
    })
  }
  colseVideo () {
    this.setState({
      showvideo: false,
      demomv: '',
      demotitle: ''
    })
  }
  prevImage(item, urls) {
    Taro.previewImage({
      current: item,
      urls: urls
    })
  }
  render () {
    const {
      result,
      bannerlist,
      navIndex,
      tipsValue,
      stagepc,
      ceremonypc,
      signpc,
      guestspc,
      lamppc,
      brightspotpc,
      levelapc,
      levelbpc,
      levelcpc,
      demomv,
      showvideo,
      demotitle
    } = this.state;
    let isvideo = false;
    if (showvideo && demomv) {
      isvideo = true;
    } else {
      isvideo = false;
    }
    return (
      <View className="page productdetail-page">
        <View className="top-banner">
          <Banner list={bannerlist}></Banner>
        </View>
        <View className="header">
          <View className="label-name">
            <View className="name">{result.name}</View>
            {
              result.labelid === 1 &&
              (<View className="label">最新</View>)
            }
            {
              result.labelid === 2 &&
              (<View className="label">最热</View>)
            }
          </View>
          <View className="price">
            <View className="discount">￥<Text className="price-txt">{result.discountmoney}</Text></View>
            <View className="old">原价:￥{result.money}</View>
          </View>
          <View className="type">
            <View className="type-name">{result.characteristicname}</View>
          </View>
          <View className="des">
            {result.introduction}
          </View>
        </View>
        <View className="white-space"></View>
        <View className="design-box">
          <View className="flow-title">
            <View className="icon"></View>
            <View className="title">产品设计</View>
            <View className="icon"></View>
          </View>
          <View className="design-content">
            <View className="design-image">
              <Image src = {result.designpc} className="image"/>
            </View>
            <View className="design-info">
              <View className="name">{result.designname}</View>
              <View className="label">资深设计师</View>
              <View className="des">{result.designintroduce}</View>
            </View>
          </View>
        </View>
        <View className="white-space"></View>
        {/* 视频 */}
        <View className="video-box">
          <View className="flow-title">
            <View className="icon"></View>
            <View className="title">产品视频</View>
            <View className="icon"></View>
          </View>
          <ScrollView
            className="video-infos"
            scrollX
            scrollWithAnimation
          >
           <View className="video-info">
              <View className="video-item" onClick={this.handleVideo.bind(this, result.productmv, '产品视频')}>
                <View className="video-image">
                  <Image src="https://www.uuwed.com/miniprogram/video-product1.jpg" className="v-image"/>
                </View>
                <View className="video-text">产品视频</View>
              </View>
              <View className="video-item" onClick={this.handleVideo.bind(this, result.designmv, '设计思路')}>
                <View className="video-image">
                  <Image src="https://www.uuwed.com/miniprogram/video-product2.jpg" className="v-image"/>
                </View>
                <View className="video-text">设计思路</View>
              </View>
              <View className="video-item video-item-last" onClick={this.handleVideo.bind(this, result.modelmv, '搭建流程')}>
                <View className="video-image">
                  <Image src="https://www.uuwed.com/miniprogram/video-product3.jpg" className="v-image"/>
                </View>
                <View className="video-text">搭建流程</View>
              </View>
           </View>
          </ScrollView>
        </View>
        <View className="white-space"></View>
        {/* 详情 */}
        <View className="product-content">
          <View className="content-title">
            <View
              className = {navIndex === 0 ? 'title title-active' : 'title'}
              onClick={this.handleClick.bind(this, 0)}
            >
              <View className="text">概述</View>
            </View>
            <View
              className = {navIndex === 1 ? 'title title-active' : 'title'}
              onClick={this.handleClick.bind(this, 1)}
            >
              <View className="text">相册</View>
            </View>
            <View
              className = {navIndex === 2 ? 'title title-active' : 'title'}
              onClick={this.handleClick.bind(this, 2)}
            >
              <View className="text">
                参数
                {
                  tipsValue > 0 && (<Text className="tips">({tipsValue}个升级配置)</Text>)
                }
              </View>
            </View>
          </View>
           {/* 概述内容 */}
          {
            navIndex === 0 && (< View className = "summary-box" >
              {
                bannerlist.map((item, index) => (
                  <View className="summary-image" key={item}>
                    <Image src= {item} className="image"/>
                  </View>
                ))
              }
            </View>)
          }
          {/* 相册内容 */}

          {
            navIndex === 1 && ( < View className = "photos-box" >
                <View className="photo-title">主舞台</View>
                <View className="photo-ul">
                  {
                    stagepc.map((item, index) => (
                      <View className="photo-li" key={item}>
                        <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, stagepc)}/>
                      </View>
                    ))
                  }
                </View>
                <View className="photo-title">仪式通道</View>
                <View className="photo-ul">
                  {
                    ceremonypc.map((item, index) => (
                      <View className="photo-li" key={item}>
                        <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, ceremonypc)}/>
                      </View>
                    ))
                  }
                </View>
                <View className="photo-title">签到台</View>
                <View className="photo-ul">
                  {
                    signpc.map((item, index) => (
                      <View className="photo-li" key={item}>
                        <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, signpc)} />
                      </View>
                    ))
                  }
                </View>
                <View className="photo-title">迎宾区</View>
                <View className="photo-ul">
                  {
                    guestspc.map((item, index) => (
                      <View className="photo-li" key={item}>
                        <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, guestspc)} />
                      </View>
                    ))
                  }
                </View>
                <View className="photo-title">舞台灯</View>
                <View className="photo-ul">
                  {
                    lamppc.map((item, index) => (
                      <View className="photo-li" key={item}>
                        <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, lamppc)} />
                      </View>
                    ))
                  }
                </View>
                {
                  brightspotpc.length && (
                    <View>
                      <View className="photo-title">其他特色</View>
                      <View className="photo-ul">
                        {
                          brightspotpc.map((item, index) => (
                            <View className="photo-li" key={item}>
                              <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, brightspotpc)} />
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }

                {
                  levelapc.length && (
                    <View>
                      <View className="photo-title">升级A套餐</View>
                      <View className="photo-ul">
                        {
                          levelapc.map((item, index) => (
                            <View className="photo-li" key={item}>
                              <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, levelapc)} />
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }

                {
                  levelbpc.length && (
                    <View>
                      <View className="photo-title">升级B套餐</View>
                      <View className="photo-ul">
                        {
                          levelbpc.map((item, index) => (
                            <View className="photo-li" key={item}>
                              <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, levelbpc)} />
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }

                {
                  levelcpc.length && (
                    <View>
                      <View className="photo-title">升级C套餐</View>
                      <View className="photo-ul">
                        {
                          levelcpc.map((item, index) => (
                            <View className="photo-li" key={item}>
                              <Image src = {item} className="photo-li-image" onClick={this.prevImage.bind(this, item, levelcpc)} />
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }
              </View>
            )
          }

          {/* 参数内容 */}
          {
            navIndex === 2 && ( < View className = "param-box" >
                <View className="param-ul">
                  <View className="param-title">主舞台</View>
                  <View className="param-image">
                    <Image src = {result.stagepcfirst} className="image"/>
                  </View>
                  <View className="param-des-box">
                    {

                      result.stagedes && result.stagedes.map((item) => (
                        <View className="param-des" key={item}>
                          {item}
                        </View>
                      ))
                    }
                  </View>
                </View>
                <View className="param-ul">
                  <View className="param-title">仪式通道</View>
                  <View className="param-image">
                    <Image src = {result.ceremonypcfirst} className="image"/>
                  </View>
                  <View className="param-des-box">
                    {

                      result.ceremonydes && result.ceremonydes.map((item) => (
                        <View className="param-des" key={item}>
                          {item}
                        </View>
                      ))
                    }
                  </View>
                </View>
                <View className="param-ul">
                  <View className="param-title">签到台</View>
                  <View className="param-image">
                    <Image src = {result.signpcfirst} className="image"/>
                  </View>
                  <View className="param-des-box">
                    {

                      result.signdes && result.signdes.map((item) => (
                        <View className="param-des" key={item}>
                          {item}
                        </View>
                      ))
                    }
                  </View>
                </View>
                <View className="param-ul">
                  <View className="param-title">迎宾区</View>
                  <View className="param-image">
                    <Image src = {result.guestspcfirst} className="image"/>
                  </View>
                  <View className="param-des-box">
                    {

                      result.guestsdes && result.guestsdes.map((item) => (
                        <View className="param-des" key={item}>
                          {item}
                        </View>
                      ))
                    }
                  </View>
                </View>
                <View className="param-ul">
                  <View className="param-title">舞台灯</View>
                  <View className="param-image">
                    <Image src = {result.lamppcfirst} className="image"/>
                  </View>
                  <View className="param-des-box">
                    {

                      result.lampdes && result.lampdes.map((item) => (
                        <View className="param-des" key={item}>
                          {item}
                        </View>
                      ))
                    }
                  </View>
                </View>
                {
                  result.brightspotpcfirst && (
                    <View className = "param-ul" >
                      <View className="param-title">其他特色</View>
                      <View className="param-image">
                        <Image src = {result.brightspotpcfirst} className="image"/>
                      </View>
                      <View className="param-des-box">
                        {

                          result.brightspotdes && result.brightspotdes.map((item) => (
                            <View className="param-des" key={item}>
                              {item}
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }
                {
                  result.levelapcfirst && (
                    <View className = "param-ul" >
                      <View className="param-title">升级A套餐</View>
                      <View className="param-image">
                        <Image src = {result.levelapcfirst} className="image"/>
                      </View>
                      <View className="param-des-box">
                        {
                          result.levelades && result.levelades.map((item) => (
                            <View className="param-des" key={item}>
                              {item}
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }
                {
                  result.levelbpcfirst && (
                    <View className = "param-ul" >
                      <View className="param-title">升级B套餐</View>
                      <View className="param-image">
                        <Image src = {result.levelbpcfirst} className="image"/>
                      </View>
                      <View className="param-des-box">
                        {
                          result.levelbdes && result.levelbdes.map((item) => (
                            <View className="param-des" key={item}>
                              {item}
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }
                {
                  result.levelcpcfirst && (
                    <View className = "param-ul" >
                      <View className="param-title">升级C套餐</View>
                      <View className="param-image">
                        <Image src = {result.levelcpcfirst} className="image"/>
                      </View>
                      <View className="param-des-box">
                        {
                          result.levelcdes && result.levelcdes.map((item) => (
                            <View className="param-des" key={item}>
                              {item}
                            </View>
                          ))
                        }
                      </View>
                    </View>
                  )
                }
            </View>)
          }
        </View>
        {
          isvideo && (
            <View className="video-play-box">
              <View className="video-play-width">
                <Image src={closeicon} className="video-icon" onClick={this.colseVideo.bind(this)}/>
                <View className="video-play-title">
                  {demotitle}
                </View>
                <Video
                  className="video-play"
                  src={demomv}
                  controls={true}
                  autoplay={true}
                  loop={false}
                  muted={false}
                ></Video>
              </View>
            </View>
          )
        }

      </View>
    )
  }
}

export default ProductDetail
