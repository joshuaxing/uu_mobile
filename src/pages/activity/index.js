/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-23 13:30:32
 * @LastEditTime: 2019-08-20 17:09:57
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import { getWindowHeight } from '../../utils/style.js'


@inject('counterStore')
@observer
class Activity extends Component {
  config = {
    navigationBarTitleText: '活动礼堂'
  }
  state = {
    imageList: [
      {
        id: 0,
        path: 'https://www.uuwed.com/miniprogram/huodong1.png'
      },
      {
        id: 1,
        path: 'https://www.uuwed.com/miniprogram/huodong2.png'
      },
      {
        id: 2,
        path: 'https://www.uuwed.com/miniprogram/huodong3.png'
      }, {
        id: 3,
        path: 'https://www.uuwed.com/miniprogram/huodong4.png'
      },
      {
        id: 4,
        path: 'https://www.uuwed.com/miniprogram/huodong5.png'
      }, {
        id: 5,
        path: 'https://www.uuwed.com/miniprogram/huodong6.png'
      }
    ]
  }

  componentWillMount () {
    this.gotData()
  }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  gotData () {

  }
  loadRecommend () {}
  prevImage (item) {
    const { imageList } = this.state;
    let urls = imageList.map((item) => {
      return item.path
    })
    Taro.previewImage({
      current: item.path,
      urls: urls
    })
  }
  render () {
    const { imageList} = this.state;
    return (
      <View className='page activity-page'>
        <View className='activity-page-content'>
          <ScrollView
            className='content-wrapper'
            scrollY
            style={{ height: getWindowHeight(false, 0)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          >
            {
              imageList.map((item) =>(
                <View className='activity-item' key={item.id}>
                  <Image src={item.path} className='activity-item-img' lazyLoad='true' onClick={this.prevImage.bind(this, item)}/>
                </View>
              ))
            }

          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Activity
