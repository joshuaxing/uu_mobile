/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-18 14:20:28
 * @LastEditTime: 2019-09-06 09:47:33
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text, ScrollView} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import Header from '../../components/header/index.js'
import Banner from './banner/index.js'
import SpaceBanner from './spacebanner/index.js'
import CaseBanner from './casebanner/index.js'
import './index.scss'
import http from '../../http/main.js'
import { getWindowHeight } from '../../utils/style.js'
@inject("counterStore")
@observer
class Index extends Component {
  config = {
    navigationBarTitleText: "优品婚匠",
    enablePullDownRefresh: true,
    onReachBottomDistance: 0
  };
  state = {
    bannerlist: [
      {
        id: 0,
        path: "https://www.uuwed.com/miniprogram/activity1.png",
        url: "/pages/product/index"
      },
      {
        id: 1,
        path: "https://www.uuwed.com/miniprogram/activity2.png",
        url: "/pages/hotel/index"
      },
      {
        id: 2,
        path: "https://www.uuwed.com/miniprogram/activity3.png",
        url: "/pages/car/index"
      },
      {
        id: 3,
        path: "https://www.uuwed.com/miniprogram/activity4.png",
        url: "/pages/case/index"
      }
    ],
    merchantdata: [],
    hoteldata: [],
    prodata: [],
    demodata: [],
    cardata: [],
    navList: [
      {
        image: "https://www.uuwed.com/miniprogram/door.png",
        value: "直营店铺",
        url: "/pages/door/index"
      },
      {
        image: "https://www.uuwed.com/miniprogram/anli.png",
        value: "精品案例",
        url: "/pages/case/index"
      },
      {
        image: "https://www.uuwed.com/miniprogram/activity.png",
        value: "活动礼堂",
        url: "/pages/activity/index"
      },
      {
        image: "https://www.uuwed.com/miniprogram/call.png",
        value: "服务电话",
        url: ""
      }
    ]
  };
  componentWillMount() {
    this.gotData();
  }

  componentWillReact() {

    this.gotData();
  }

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  onPullDownRefresh() {
    this.gotData();
  }

  handleMoreDoor() {
    Taro.navigateTo({
      url: "/pages/door/index"
    });
  }
  handleMoreProduct() {
    Taro.switchTab({
      url: "/pages/product/index"
    });
  }
  handleMoreCase() {
    Taro.navigateTo({
      url: "/pages/case/index"
    });
  }
  handleMoreHotel() {
    Taro.switchTab({
      url: "/pages/hotel/index"
    });
  }
  handleMoreCar() {
    Taro.switchTab({
      url: "/pages/car/index"
    });
  }
  handleOrder() {
    Taro.navigateTo({
      url: "/pages/orderstore/index"
    });
  }
  handleGrid(item, index) {
    if (index !== 3) {
      Taro.navigateTo({
        url: item.url
      });
    } else {
      Taro.makePhoneCall({
        phoneNumber: "4008850525"
      })
        .then(suc => {})
        .catch(error => {});
    }
  }
  gotData() {
    const { currentcity } = this.props.counterStore;
    http("/app/homelist.do", {
      cityid: currentcity.id,
      areaid: currentcity.areaid
    }).then(result => {
      if (result.status === 1) {
        const data = result.data;
        this.setState({
          merchantdata: data.merchantdata,
          hoteldata: data.hoteldata,
          prodata: data.prodata,
          demodata: data.demodara,
          cardata: data.cardara
        });
      }
      Taro.stopPullDownRefresh();
    });
  }
  handleDetail(item) {
    Taro.navigateTo({
      url: `/pages/hoteldetail/index?id=${item.hotelid}`
    });
  }
  handleCarDetail(item) {
    Taro.navigateTo({
      url: `/pages/cardetail/index?id=${item.carid}`
    });
  }
  gotCarName(value) {
    if (!value.length) return "";
    return value[0].name;
  }
  autoUpdate() {
    // 获取小程序更新机制兼容
    if (Taro.canIUse("getUpdateManager")) {
      // 检查小程序是否有新版本发布
      const updateManager = Taro.getUpdateManager();
      updateManager.onCheckForUpdate(res => {
        // 请求完新版本信息的回调
        console.log("请求完新版本信息的回调");
        if (res.hasUpdate) {
          console.log("有新版本");
          // 小程序有新版本，则静默下载新版本，做好更新准备
          updateManager.onUpdateReady(() => {
            Taro.showModal({
              title: "更新提示",
              content: "新版本已经准备好，请重启应用",
              showCancel: false
            }).then(res => {
              if (res.confirm) {
                updateManager.applyUpdate();
              }
            });
          });
          // 新版本下载失败
          updateManager.onUpdateFailed(function() {
            console.log("失败");
            Taro.showModal({
              title: "已经有新版本了哟~",
              content: "新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~"
            });
          });
        }
      });
    } else {
      Taro.showModal({
        title: "提示",
        content: "当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试"
      });
    }
  }
  render() {
    const {
      bannerlist,
      merchantdata,
      hoteldata,
      prodata,
      demodata,
      cardata,
      navList
    } = this.state;
    const {
      counterStore: { currentcity }
    } = this.props;
    return (
      <View className="page">
        <Header
          cityname={currentcity.name}
          areaname={currentcity.areaname}
        ></Header>
        <ScrollView
          className="page-content"
          scrollY
          style={{ height: getWindowHeight(true, 0) }}
        >
          <Banner list={bannerlist}></Banner>
          <View className="home-grids">
            <View className="grids-ul">
              {navList.map((item, index) => (
                <View
                  className="grids-li"
                  onClick={this.handleGrid.bind(this, item, index)}
                  key={item.url}
                >
                  <Image src={item.image} className="grids-li-image" />
                  <View className="grids-li-text">{item.value}</View>
                </View>
              ))}
            </View>
          </View>
          <View className="space-pink"></View>
          <View className="order-box" onClick={this.handleOrder}>
            <Image
              src="https://www.uuwed.com/miniprogram/yuyue.png"
              className="order-image"
            />
          </View>
          <View className="content-title">
            <View className="title-h1">精选婚庆店铺</View>
            <Text className="title-h2">消费透明/专业团队/服务舒心</Text>
          </View>
          <View className="door-part">
            {merchantdata.map((item, index) => (
              <View className="door-list" key={item.merchantid}>
                <View className="list-title">
                  <Text className="title-label">{item.name}</Text>
                  <Text className="title-name">{item.typename}</Text>
                </View>
                <Image src={item.image} className="list-image" />
                <View className="list-contents">
                  <View className="list-content">
                    <View className="con-label">店铺地址：</View>
                    <View className="con-name">{item.address}</View>
                  </View>
                  <View className="list-content">
                    <View className="con-label">店铺交通：</View>
                    <View className="con-name">{item.traffic}</View>
                  </View>
                </View>
              </View>
            ))}
          </View>
          <View className="more-btn" onClick={this.handleMoreDoor}>
            <View className="more-btn-txt">查看更多</View>
            <View className="more-btn-icon at-icon at-icon-add"></View>
          </View>
          <View className="space-row"></View>
          <View className="procuct-part">
            <View className="content-title-small">
              <View className="title-left">
                <Text className="txt">精选婚礼产品</Text>
              </View>
              <View className="title-right" onClick={this.handleMoreProduct}>
                <Text className="txt">查看更多</Text>
                <View className="title-icon at-icon at-icon-chevron-right"></View>
              </View>
            </View>
            <SpaceBanner list={prodata}></SpaceBanner>
          </View>
          <View className="space-row"></View>
          <View className="case-part">
            <View className="content-title-small">
              <View className="title-left">
                <Text className="txt">精选婚礼案例</Text>
              </View>
              <View className="title-right" onClick={this.handleMoreCase}>
                <Text className="txt">查看更多</Text>
                <View className="title-icon at-icon at-icon-chevron-right"></View>
              </View>
            </View>
            <CaseBanner list={demodata}></CaseBanner>
          </View>
          <View className="content-title">
            <View className="title-h1">精选品质酒店</View>
            <Text className="title-h2">-精选酒店 品质为你-</Text>
          </View>
          <View className="hotel-part standard-part">
            <View className="list-ul">
              {hoteldata.map((item, index) => (
                <View
                  className="list-li"
                  key={item.hotelid}
                  onClick={this.handleDetail.bind(this, item)}
                >
                  <Image
                    src={item.hotelimage}
                    className="list-li-image"
                  ></Image>
                  <View className="name">{item.hotelname}</View>
                  <View className="table">
                    <Text>可容纳{item.maxtable}桌</Text>
                    <Text className="line">|</Text>
                    <Text>{item.hotelposition}</Text>
                    <Text className="line">|</Text>
                    <Text>{item.hotellevel}</Text>
                  </View>
                  <View className="price">
                    ￥{item.price}
                    <Text className="price-txt">起</Text>
                  </View>
                </View>
              ))}
            </View>
          </View>
          <View className="more-btn" onClick={this.handleMoreHotel}>
            <View className="more-btn-txt">查看更多</View>
            <View className="more-btn-icon at-icon at-icon-add"></View>
          </View>
          <View className="content-title">
            <View className="title-h1">精选品牌酒店</View>
            <Text className="title-h2">-精选好车 品质为你-</Text>
          </View>
          <View className="car-part standard-part">
            <View className="list-ul">
              {cardata.map((item, index) => (
                <View
                  className="list-li"
                  key={item.carid}
                  onClick={this.handleCarDetail.bind(this, item)}
                >
                  <Image src={item.carimage} class="list-li-image"></Image>
                  <View className="name">{item.name}</View>
                  <View className="des">
                    <Text className="des-txt">车身颜色</Text>
                    <Text>{this.gotCarName(item.colordata)}</Text>
                  </View>
                  <View className="price">
                    ￥{item.price}
                    <Text className="price-txt">起</Text>
                  </View>
                </View>
              ))}
            </View>
          </View>
          <View className="more-btn last-more-btn" onClick={this.handleMoreCar}>
            <View className="more-btn-txt">查看更多</View>
            <View className="more-btn-icon at-icon at-icon-add"></View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Index
