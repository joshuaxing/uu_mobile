/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-19 17:48:16
 * @LastEditTime: 2019-08-20 12:49:36
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('counterStore')
@observer
class SwiperBanner extends Component {

  static defaultProps = {
    list: []
  }
  componentWillMount () { }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  goPage (url, index) {
    if (index === 3) {
      Taro.navigateTo({
        url: url
      })
    } else {
      Taro.switchTab({
        url: url
      })
    }
  }

  render () {
    const { list } = this.props
    return (
      <View className='home-banner'>
        <Swiper
          className='home-banner__swiper'
          circular
          autoplay
          indicatorDots
          indicatorColor = 'rgb(255,255,255,1)'
          indicatorActiveColor = 'rgb(252,71,71)'
          // TODO 目前 H5、RN 暂不支持 previousMargin、nextMargin
          // previousMargin
          // nextMargin
        >
          {list.map((item, index)=> (
            <SwiperItem
              key={item.id}
              className='home-banner__swiper-item'
            >
              <Image
                className='home-banner__swiper-item-img'
                src={item.path}
                onClick={this.goPage.bind(this, item.url, index)}
              />
            </SwiperItem>
          ))}
        </Swiper>
      </View>
    )
  }
}

export default SwiperBanner
