/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-20 15:38:28
 * @LastEditTime: 2019-08-22 09:31:16
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image, Text} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('counterStore')
@observer
class SpaceBanner extends Component {

  static defaultProps = {
    list: []
  }
  componentWillMount () { }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  handleDetail(item) {
    Taro.navigateTo({
      url: `/pages/casedetail/index?id=${item.demoid}`
    })
  }

  render () {
    const { list } = this.props
    return (
      <View className='case-banner'>
        <Swiper
          className='case-banner__swiper'
          indicatorDots
          indicatorColor = 'rgb(102,102,102,1)'
          indicatorActiveColor = 'rgb(252,71,71)'
          // TODO 目前 H5、RN 暂不支持 previousMargin、nextMargin
          // previousMargin
          // nextMargin
        >
          {list.map((item, index)=> (
            <SwiperItem
              key={item.id}
              className='case-banner__swiper-item'
            >
              <View className="swiper-item-box"  onClick={(e) => this.handleDetail(item, e)}>
                < Image
                  className = 'swiper-item-img'
                  src = {
                    item.image
                  }
                  />
                <View className="name">{item.name}</View>
                <View className="ad">
                  <Text>{item.demoposition}</Text>
                  <Text className="line">|</Text>
                  <Text>{item.hotelmsg}</Text>
                </View>
                <View className="ad">{item.demotime}</View>
                <View className="des">{item.demodescribe}</View>
              </View>
            </SwiperItem>
          ))}
        </Swiper>
      </View>
    )
  }
}

export default SpaceBanner
