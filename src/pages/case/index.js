/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-23 13:30:32
 * @LastEditTime: 2019-08-09 15:01:35
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, Swiper, SwiperItem} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import Header from '../../components/header/index.js'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class Case extends Component {
  tableLiHeight = 290
  config = {
    navigationBarTitleText: '精品案例'
  }
  navList = [
    {id: 0, name: '全部'},
    {id: 4, name: '标准'},
    {id: 5, name: '高端'}
  ]
  state = {
    totalpage: 0,
    alltotalpage: 0,
    bztotalpage: 0,
    gdtotalpage: 0,
    page: 1,
    limit: 8,
    type: 0,
    nodata: true,
    loading: false,
    hasMore: true,
    result: [],
    resultAll: [],
    resultBZ: [],
    resultGD: [],
    tableHeight: 150,
    tableIndex: 0
  }

  componentWillMount () { 
    this.gotData()
  }

  componentWillReact () {
    this.setState({
      result: [],
      resultAll: [],
      resultBZ: [],
      resultGD: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  handleType (item) {
    const { resultAll, resultBZ, resultGD, alltotalpage, bztotalpage, gdtotalpage} = this.state;
    if (item.id === 0) {
      !resultAll.length ?
        this.setState({
          tableIndex: 0,
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          tableHeight: (resultAll.length * this.tableLiHeight),
          result: resultAll,
          tableIndex: 0,
          type: item.id,
          totalpage: alltotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    } else if (item.id === 4) {
      !resultBZ.length ?
        this.setState({
          tableIndex: 1,
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          tableHeight: (resultBZ.length * this.tableLiHeight),
          result: resultBZ,
          tableIndex: 1,
          type: item.id,
          totalpage: bztotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    } else {
      !resultGD.length ?
        this.setState({
          result: [],
          page: 1,
          loading: false,
          hasMore: true,
          nodata: true,
          tableIndex: 2,
          type: item.id
        }, () => {
          this.gotData();
        }) :
        this.setState({
          tableHeight: (resultGD.length * this.tableLiHeight),
          result: resultGD,
          tableIndex: 2,
          type: item.id,
          totalpage: gdtotalpage,
          loading: false,
          hasMore: true,
          nodata: true
        })
    }
  }
  gotData () {
    //列表数据
    const { counterStore: { currentcity } } = this.props
    const { page, limit, hasMore, loading, type} = this.state;
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true })
    http('/app/demolistbycity.do', {
      cityid: currentcity.id,
      areaid: currentcity.areaid,
      page: page,
      limit: limit,
      type: type
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (type === 0) {
          this.setState({
            resultAll: alldata,
            alltotalpage: totalpage
          })
        } else if (type === 4) {
          this.setState({
            resultBZ: alldata,
            bztotalpage: totalpage
          })
        } else {
          this.setState({
            resultGD: alldata,
            gdtotalpage: totalpage
          })
        }
        if (alldata.length) {
          this.setState({
            nodata: true,
            result: alldata,
            loading: false,
            totalpage: totalpage,
            tableHeight: (alldata.length * this.tableLiHeight)
          })
        } else {
          this.setState({
            nodata: false,
            result: alldata,
            loading: false,
            totalpage: totalpage,
            tableHeight: 15
          })
        }
      }
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
  changeTable(event) {
    const current = event.detail.current;
    this.handleType(this.navList[current])
  }
  handleDetail(item) {
    Taro.navigateTo({
      url: `/pages/casedetail/index?id=${item.id}`
    })
  }
  render () {
    const { tableHeight, tableIndex, type, nodata, resultAll, resultGD, resultBZ} = this.state;
    const { counterStore: { currentcity } } = this.props;
    // console.log(tableHeight)
    return (
      <View className="page case-page">
        <Header cityname={currentcity.name} areaname= {currentcity.areaname}></Header>
        <View className="case-page-content">
          <View className="page-title">
            {
              this.navList.map((item, index) => (
                <View 
                  className={item.id === type ? 'title case-title-active' : 'title'}
                  key = {item.id}
                  onClick = {(e) => this.handleType(item,e)}
                >
                  {item.name}
                </View>
              ))
            }
          </View>
          <ScrollView 
            className="content-wrapper"
            scrollY
            style={{ height: getWindowHeight(false, 95)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          > 
            <Swiper 
              className="table-box-swiper" 
              current={tableIndex}
              style={{ height: tableHeight + 'px'}}
              onChange = {this.changeTable.bind(this)}
            >
              <SwiperItem>
                <View className="case-table-box">
                  {
                    resultAll.map((item, index) => (
                      <View className="case-table-li" 
                        key={item.id} 
                        onClick={(e) => this.handleDetail(item, e)}
                      >
                        <Image src={item.pcpath[0]} className="case-li-image"/>
                        <View className="case-li-content">
                          <View className="name">
                            <View className="name-title">{item.name}</View>
                            {
                              item.labelid === 4 &&
                              (<View className="name-lable">标准</View>) 
                            }
                            {
                              item.labelid === 5 &&
                              (<View className="name-lable">高端</View>)
                            }
                          </View>
                          <View className="regionname">{item.regionname}</View>
                          <View className="des">{item.introduction}</View>
                        </View>
                      </View>
                    ))
                  }
                </View>
               </SwiperItem>
               <SwiperItem>
                <View className="case-table-box">
                  {
                    resultBZ.map((item, index) => (
                      <View className="case-table-li" 
                        key={item.id} 
                        onClick={(e) => this.handleDetail(item, e)}
                      >
                        <Image src={item.pcpath[0]} className="case-li-image"/>
                        <View className="case-li-content">
                          <View className="name">
                            <View className="name-title">{item.name}</View>
                            {
                              item.labelid === 4 &&
                              (<View className="name-lable">标准</View>) 
                            }
                            {
                              item.labelid === 5 &&
                              (<View className="name-lable">高端</View>)
                            }
                          </View>
                          <View className="regionname">{item.regionname}</View>
                          <View className="des">{item.introduction}</View>
                        </View>
                      </View>
                    ))
                  }
                </View>
               </SwiperItem>
               <SwiperItem>
                <View className="case-table-box">
                  {
                    resultGD.map((item, index) => (
                      <View className="case-table-li" 
                        key={item.id} 
                        onClick={(e) => this.handleDetail(item, e)}
                      >
                        <Image src={item.pcpath[0]} className="case-li-image"/>
                        <View className="case-li-content">
                          <View className="name">
                            <View className="name-title">{item.name}</View>
                            {
                              item.labelid === 4 &&
                              (<View className="name-lable">标准</View>) 
                            }
                            {
                              item.labelid === 5 &&
                              (<View className="name-lable">高端</View>)
                            }
                          </View>
                          <View className="regionname">{item.regionname}</View>
                          <View className="des">{item.introduction}</View>
                        </View>
                      </View>
                    ))
                  }
                </View>
               </SwiperItem>
            </Swiper>
            {
              !nodata && <Nodata></Nodata>
            }
            {this.state.loading &&
              <View className='home__loading'>
                <Text className='home__loading-txt'>正在加载中...</Text>
              </View>
            }
            {!this.state.hasMore &&
              <View className='home__loading home__loading--not-more'>
                <Text className='home__loading-txt'>没有更多内容了</Text>
              </View>
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Case
