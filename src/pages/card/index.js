import Taro, { Component } from "@tarojs/taro";
import { View, Swiper, Image, Text, Audio} from "@tarojs/components";
import { observer, inject } from "@tarojs/mobx";
import { getWindowHeight } from "../../utils/style.js";
import "./index.scss";
import "animate.css";
import bg from "./bg.jpg";
import firstleft from "./first-left.png";
import firstright from "./first-right.png";
import jiantou from "./jiantou-icon.png";
import xinicon from "./xin-icon.png";
import firstbottom from "./first-bottom.png";
import firsttips from "./first-tips.png";
import weddingtxt from "./wedding-icon.png";
import andtxt from "./and-icon.png";
import musicon from "./music-on.png";
import musicoff from "./music-off.png";
let baseurl = 'https://www.uuwed.com/weddingcard/card1'
@inject("counterStore")
@observer
class Card extends Component {
  config = {
    navigationBarTitleText: "电子请柬"
  };
  state = {
    current: 0,
    isplay: 'play'
  };
  componentWillMount() {}

  componentWillReact() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  handleChange(e) {
    this.setState({
      current: e.detail.current
    });
  }
  handleMusic () {
    const isplayvalue = this.state.isplay;
    let isplay = isplayvalue === 'play' ? 'pause' : 'play';
    this.setState({
      isplay: isplay
    })
  }
  render() {
    const { current, isplay} = this.state;
    return (
      <View style={{ height: getWindowHeight(false, 0) }}>
        <Swiper
          className="card-list-box"
          vertical
          onChange={this.handleChange.bind(this)}
          current={current}
        >
          {/* 第1页start */}
          <SwiperItem className="card-list">
            <Image src={bg} className="card-list-image" />
            <View className="card-list-item">
              <View className="name-box">
                <Image
                  src={firstleft}
                  className={`name-image animated ${
                    current === 0 ? "fadeInLeft" : ""
                  }`}
                />
                <View className="name-content">
                  <Image src={weddingtxt} className="wedding-txt" />
                  <View
                    className={`wedding-people animated ${
                      current === 0 ? "zoomInDown delay-1s" : ""
                    }`}
                  >
                    <Text className="txt">MR.</Text>余淮
                  </View>
                  <Image src={andtxt} className="and-txt" />
                  <View
                    className={`wedding-people animated ${
                      current === 0 ? "zoomInUp delay-1s" : ""
                    }`}
                  >
                    <Text className="txt">MISS.</Text>耿耿
                  </View>
                </View>
                <Image
                  src={firstright}
                  className={`name-image animated ${
                    current === 0 ? "fadeInRight" : ""
                  }`}
                />
              </View>
              <View className="time-box">
                <View class="time-jiantou-box">
                  <Image src={jiantou} className="time-jiantou" />
                </View>
                <View className={`time-xin`}>
                  <Image
                    src={xinicon}
                    className={`time-xin-image animated ${
                      current === 0 ? "zoomIn delay-2s" : ""
                    }`}
                  />
                  <View
                    className={`time-xin-txt animated ${
                      current === 0 ? "zoomIn delay-2s" : ""
                    }`}
                  >
                    2019年
                  </View>
                </View>
              </View>
              <View
                className={`time-text animated ${
                  current === 0 ? "slideInLeft delay-2s" : ""
                }`}
              >
                10月10日 上午12：00
              </View>
              <Image src={firsttips} className="time-tips" />
            </View>
            <Image
              src={firstbottom}
              className={`first-bottom-image animated ${
                current === 0 ? "fadeInUpBig" : ""
              }`}
            />
          </SwiperItem>
          {/* 第1页end */}
          {/* 第2页start */}
          <SwiperItem className="card-list">
            <Image src={bg} className="card-list-image" />
            <View className="card-list-item card-list-flex">
              <View className="list-item-content">
                <View className="content-box">
                  <View
                    className={`zhaopian-box animated ${
                      current === 1 ? "slideInLeft" : ""
                    }`}
                  >
                    <Image
                      src={`${baseurl}/xinlang.jpg`}
                      className={`zhaopian-image animated ${
                        current === 1 ? "rotateInDownRight delay-1s" : ""
                      }`}
                    />
                  </View>
                  <View className="people-name-box">
                    <Image src={`${baseurl}/content-hua.png`} className="people-name-image" />
                    <View
                      className={`people-name-des animated ${
                        current === 1 ? "fadeInRight delay-1s" : ""
                      }`}
                    >
                      新郎：
                    </View>
                  </View>
                  <View className="zhufu-content">
                    <Image src={`${baseurl}/line.png`} className="zhufu-title" />
                    <View
                      className={`zhufu-des animated ${
                        current === 1 ? "fadeInUpBig delay-1s" : ""
                      }`}
                    >
                      愿得一人心，白首不相离 从此以后我的世界只有你
                    </View>
                  </View>
                </View>
                <Image
                  src={`${baseurl}/second-left-icon.png`}
                  className={`second-left-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
                <Image
                  src={`${baseurl}/second-right-icon.png`}
                  className={`second-right-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
              </View>
            </View>
            <Image src={`${baseurl}/second-top-bg.png`} className={`second-top-bg`} />
            <Image src={`${baseurl}/second-bottom-bg.png`} className={`second-bottom-bg`} />
          </SwiperItem>
           {/* 第2页end */}
            {/* 第3页start */}
          <SwiperItem className="card-list">
            <Image src={bg} className="card-list-image" />
            <View className="card-list-item card-list-flex">
              <View className="list-item-content">
                <View className="content-box">
                  <View
                    className={`zhaopian-box animated ${
                      current === 2 ? "slideInLeft" : ""
                    }`}
                  >
                    <Image
                      src={`${baseurl}/xinniang.jpg`}
                      className={`zhaopian-image animated ${
                        current === 2 ? "rotateInDownLeft delay-1s" : ""
                      }`}
                    />
                  </View>
                  <View className="people-name-box">
                  <Image src={`${baseurl}/content-hua.png`} className="people-name-image" />
                    <View
                      className={`people-name-des animated ${
                        current === 2 ? "fadeInRight delay-1s" : ""
                      }`}
                    >
                      新娘：
                    </View>
                  </View>
                  <View className="zhufu-content">
                  <Image src={`${baseurl}/line.png`} className="zhufu-title" />
                    <View
                      className={`zhufu-des animated ${
                        current === 2 ? "fadeInUpBig delay-1s" : ""
                      }`}
                    >
                      这一天我将穿上婚纱迎接我们幸福的起点
                    </View>
                  </View>
                </View>
                <Image
                  src={`${baseurl}/second-left-icon.png`}
                  className={`second-left-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
                <Image
                  src={`${baseurl}/second-right-icon.png`}
                  className={`second-right-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
              </View>
            </View>
            <Image src={`${baseurl}/second-top-bg.png`} className={`second-top-bg`} />
            <Image src={`${baseurl}/second-bottom-bg.png`} className={`second-bottom-bg`} />
          </SwiperItem>
           {/* 第3页end */}
          {/* 第4页start */}
          <SwiperItem className="card-list">
            <Image src={bg} className="card-list-image" />
            <View className="card-list-item card-list-flex">
              <View className="list-item-content">
                <View className="content-box content-box-other">
                  <View
                    className={`hezhao-box animated ${
                      current === 3 ? "rotateIn" : ""
                    }`}
                  >
                    <Image src={`${baseurl}/hezhao1.png`} className={`hezhao-image`} />
                  </View>
                  <View
                    className={`hezhao-des animated ${
                      current === 3 ? "zoomInDown" : ""
                    }`}
                  >
                    爱对了人情人节每天过，我们最好的时间遇见最对的彼此
                  </View>
                  <View className="hezhao-other">
                    <View
                      className={`hezhao-other-left animated ${
                        current === 3 ? "slideInLeft" : ""
                      }`}
                    >
                      <Image src={`${baseurl}/hezhao2.png`} className="hezhao-left" />
                    </View>
                    <View className="hezhao-other-right">
                      <View
                        className={`hezhao-right-box animated ${
                          current === 3 ? "slideInRight" : ""
                        }`}
                      >
                        <Image src={`${baseurl}/hezhao3.png`} className="hezhao-right" />
                      </View>
                      <Image
                        src={`${baseurl}/huahuai.png`}
                        className={`hezhao-right-icon animated ${
                          current === 3 ? "zoomIn" : ""
                        }`}
                      />
                    </View>
                  </View>
                </View>
                <Image
                  src={`${baseurl}/second-left-icon.png`}
                  className={`second-left-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
                <Image
                  src={`${baseurl}/second-right-icon.png`}
                  className={`second-right-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
              </View>
            </View>
            <Image src={`${baseurl}/second-top-bg.png`} className={`second-top-bg`} />
            <Image src={`${baseurl}/second-bottom-bg.png`} className={`second-bottom-bg`} />
          </SwiperItem>
           {/* 第4页end */}
           {/* 第5页start */}
          <SwiperItem className="card-list">
            <Image src={bg} className="card-list-image" />
            <View className="card-list-item card-list-flex">
              <View className="list-item-content">
                <View className="content-box content-box-five">
                  <View
                    className={`hezhao-four-box animated ${
                      current === 4 ? "slideInLeft" : ""
                    }`}
                  >
                    <Image
                      src={`${baseurl}/hezhao4.png`}
                      className={`hezhao-four-image animated ${
                        current === 4 ? "zoomIn delay-1s" : ""
                      }`}
                    />
                  </View>
                  <Image
                    src={`${baseurl}/end-icon.png`}
                    className={`four-end-image animated ${
                      current === 4 ? "fadeInDownBig delay-1s" : ""
                    }`}
                  />
                </View>
                <Image
                  src={`${baseurl}/second-left-icon.png`}
                  className={`second-left-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
                <Image
                  src={`${baseurl}/second-right-icon.png`}
                  className={`second-right-icon animated ${
                    current === 1 ? "fadeIn" : ""
                  }`}
                />
              </View>
            </View>
            <Image src={`${baseurl}/second-top-bg.png`} className={`second-top-bg`} />
            <Image src={`${baseurl}/second-bottom-bg.png`} className={`second-bottom-bg`} />
          </SwiperItem>
          {/* 第5页end */}
          {/*第6页start */}
          <SwiperItem className="card-list">
            <Image src={bg} className="card-list-image" />
            <View className="card-list-item">
              <View className="name-box">
                <Image
                  src={`${baseurl}/first-left.png`}
                  className={`name-image animated ${
                    current === 5 ? "fadeInLeft" : ""
                  }`}
                />
                <View className="name-content">
                  <Image src={`${baseurl}/wedding-icon.png`} className="wedding-txt" />
                  <View class="huanying-text huanying-text1">诚邀您参加</View>
                  <View class="huanying-text huanying-text2">我们的婚礼</View>
                </View>
                <Image
                  src={`${baseurl}/first-right.png`}
                  className={`name-image animated ${
                    current === 5 ? "fadeInRight" : ""
                  }`}
                />
              </View>
              <View  className={`five-content-box animated ${
                    current === 5 ? "slideInLeft" : ""
                  }`}>
                <View
                  className={`five-content-text`}
                >
                  新郎：余淮  新娘：耿耿
                </View>
                <View
                  className={`five-content-text`}
                >
                  2019年10月10日 上午12：00
                </View>
                <View
                  className={`five-content-text`}
                >
                  地址：浦东大道200号玫瑰酒店玫瑰厅
                </View>
              </View>
              <Image src={`${baseurl}/map.png`} className={`address-image animated ${
                  current === 5 ? "fadeInUp" : ""
              }`}/>
            </View>
            <Image
              src={`${baseurl}/first-bottom.png`}
              className={`first-bottom-image animated ${
                current === 5 ? "fadeInUpBig" : ""
              }`}
            />
          </SwiperItem>
          {/* 第6页end */}
        </Swiper>
        <Audio 
          src={`${baseurl}/love.mp3`}
          controls={false}
          autoplay={true}
          loop={true}
          className="my-audio"
          action={{method: isplay}}
          ></Audio>
          <Image src={ isplay === 'play' ? musicon : musicoff} className="audio-icon" onClick={this.handleMusic}/>
      </View>
    );
  }
}

export default Card;
