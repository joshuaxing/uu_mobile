import Taro, { Component, connectSocket } from "@tarojs/taro";
import { View, Image, ScrollView } from "@tarojs/components";
import { observer, inject } from "@tarojs/mobx";
import { formatDate, formatWeek } from "../../utils/hanldeDate.js";
import calendar from "../../utils/calendar.js";
import { getWindowHeight } from "../../utils/style.js";
import http from "../../http/main.js";
import "./index.scss";
import closeicon from "./close-icon.png";
@inject("counterStore")
@observer
class DatePage extends Component {
  config = {
    navigationBarTitleText: "黄道吉日"
  };
  festival = {
    lunar: {
      "1-1": "春节",
      "1-15": "元宵节",
      "2-2": "龙头节",
      "5-5": "端午节",
      "7-7": "七夕节",
      "7-15": "中元节",
      "8-15": "中秋节",
      "9-9": "重阳节",
      "10-1": "寒衣节",
      "10-15": "下元节",
      "12-8": "腊八节",
      "12-23": "祭灶节"
    },
    gregorian: {
      "1-1": "元旦",
      "2-14": "情人节",
      "3-8": "妇女节",
      "3-12": "植树节",
      "4-5": "清明节",
      "5-1": "劳动节",
      "5-4": "青年节",
      "6-1": "儿童节",
      "7-1": "建党节",
      "8-1": "建军节",
      "9-10": "教师节",
      "10-1": "国庆节",
      "12-24": "平安夜",
      "12-25": "圣诞节"
    }
  };
  allcount = 42;
  state = {
    weeks: [
      {
        id: 0,
        name: "日"
      },
      {
        id: 1,
        name: "一"
      },
      {
        id: 2,
        name: "二"
      },
      {
        id: 3,
        name: "三"
      },
      {
        id: 4,
        name: "四"
      },
      {
        id: 5,
        name: "五"
      },
      {
        id: 6,
        name: "六"
      }
    ],
    lunar: true,
    curYear: "",
    curMonth: "",
    curDay: "",
    calendars: [],
    yearCount: 100,
    isshowinfo: false,
    isshow: false,
    count: 1
  };
  componentWillMount() {
    // this.init();
    
    this.computedDate(2019, 11, 1)
  }

  componentWillReact() {
    // console.log('componentWillReact')
  }

  componentDidMount() {
    // setTimeout(() => {
    //   this.setState({
    //     count: 0
    //   })
    // },1000)
  }

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  calendarFactory(year, month) {
    const { curYear, curMonth, curDay } = this.state;
    let daysCount = this.getDays(year, month); //当前月天数
    let startWeek = this.getWeek(year, month); //当月第一天星期几

    //补充前一个月
    let prevYear = year;
    let prevMonth = month - 1;
    prevMonth <= 0 && (prevMonth = 12) && (prevYear -= 1);
    let prevmonthCount = this.getDays(prevYear, prevMonth); //前一个天数
    let prevdaysAll = Array.from(new Array(prevmonthCount), (val, index) => {
      return index + 1;
    });
    let prevdaysArr = prevdaysAll.filter(item => {
      if (item > prevmonthCount - startWeek) {
        return item;
      }
    });
    let prevdays = prevdaysArr.map((item, index) => {
      let lunarInfo = this.getLunarInfo(prevYear, prevMonth, item);
      let disabled = true;
      return {
        value: item,
        week: new Date(`${prevYear}/${prevMonth}/${item}`).getDay(),
        timestamp: new Date(`${prevYear}/${prevMonth}/${item}`).getTime(),
        text: lunarInfo.lunar,
        disabled: disabled,
        isLunarFestival: lunarInfo.isLunarFestival,
        isGregorianFestival: lunarInfo.isGregorianFestival,
        istody: lunarInfo.istody,
        date: `${prevYear}-${prevMonth}-${item}`
      };
    });
    prevdays.map((item, index) => {
      this.gotisJI(item.date).then(result => {
        let isji = false;
        let ji = "";
        let yi = "";
        if (result.status === 1) {
          const data = result.data;
          data.yi.indexOf("嫁娶") !== -1 && (isji = true);
          ji = data.ji;
          yi = data.yi;
        }
        item.isji = isji;
        item.ji = ji;
        item.yi = yi;
      });
    });
    //console.log(prevdays)

    //补充后一个月
    let nextYear = year;
    let nextMonth = month + 1;
    nextMonth > 12 && (nextMonth = 1) && (prevYear += 1);

    const nextmonthCount = this.allcount - daysCount - startWeek;

    let nextdaysArr = Array.from(new Array(nextmonthCount), (val, index) => {
      return index + 1;
    });

    let nextdays = nextdaysArr.map((item, index) => {
      let lunarInfo = this.getLunarInfo(nextYear, nextMonth, item);
      let disabled = true;
      return {
        value: item,
        week: new Date(`${nextYear}/${nextMonth}/${item}`).getDay(),
        timestamp: new Date(`${nextYear}/${nextMonth}/${item}`).getTime(),
        text: lunarInfo.lunar,
        disabled: disabled,
        isLunarFestival: lunarInfo.isLunarFestival,
        isGregorianFestival: lunarInfo.isGregorianFestival,
        istody: lunarInfo.istody,
        date: `${nextYear}-${nextMonth}-${item}`
      };
    });
    nextdays.map((item, index) => {
      this.gotisJI(item.date).then(result => {
        let isji = false;
        let ji = "";
        let yi = "";
        if (result.status === 1) {
          const data = result.data;
          data.yi.indexOf("嫁娶") !== -1 && (isji = true);
          ji = data.ji;
          yi = data.yi;
        }
        item.isji = isji;
        item.ji = ji;
        item.yi = yi;
      });
    });
    // console.log(nextdays)

    //当前月
    let currentdays = Array.from(new Array(daysCount), (val, index) => {
      let lunarInfo = this.getLunarInfo(year, month, index + 1);
      let disabled = false;
      year <= curYear &&
        month <= curMonth &&
        index + 1 < curDay &&
        (disabled = true);
      return {
        value: index + 1,
        week: new Date(`${year}/${month}/${index + 1}`).getDay(),
        timestamp: new Date(`${year}/${month}/${index + 1}`).getTime(),
        text: lunarInfo.lunar,
        disabled: disabled,
        isLunarFestival: lunarInfo.isLunarFestival,
        isGregorianFestival: lunarInfo.isGregorianFestival,
        istody: lunarInfo.istody,
        date: `${year}-${month}-${index + 1}`
      };
    });
    currentdays.map((item, index) => {
      this.gotisJI(item.date).then(result => {
        let isji = false;
        let ji = "";
        let yi = "";
        if (result.status === 1) {
          const data = result.data;
          data.yi.indexOf("嫁娶") !== -1 && (isji = true);
          ji = data.ji;
          yi = data.yi;
        }
        item.isji = isji;
        item.ji = ji;
        item.yi = yi;
      });
    });
    console.log(currentdays)

    let days = [];
    days = prevdays.concat(currentdays, nextdays);

    return {
      year,
      month,
      days
    };
  }

  createCalendars() {
    let calendars = [];
    let currentYear = this.state.curYear;
    let curMonth = this.state.curMonth;
    calendars.push(this.calendarFactory(currentYear, curMonth));
    console.log(calendars)
    this.setState({
      calendars: calendars
    });
  }
  init() {
    let date = new Date();
    const curYear = parseInt(date.getFullYear(), 10);
    const curMonth = parseInt(date.getMonth(), 10) + 1; //月份+1
    const curDay = parseInt(date.getDate(), 10);
    this.computedDate(curYear, curMonth, curDay);
  }
  computedDate(curYear, curMonth, curDay) {
    this.setState(
      {
        curYear: curYear,
        curMonth: curMonth,
        curDay: curDay
      },
      () => {
        this.createCalendars();
      }
    );
  }
  gotDaysCount(year, month) {
    //获取一个月有多少天
    const isLeapYear =
      year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0) ? 1 : 0;
    const days = month === 2 ? 28 + isLeapYear : 31 - (((month - 1) % 7) % 2);
    return days;
  }
  getDays(curYear, curMonth) {
    //获取一个月多少天
    let day = new Date(curYear, curMonth, 0);
    return day.getDate();
  }

  getWeek(year, month) {
    //当月第一天星期几
    let date = new Date();
    date.setYear(year);
    date.setMonth(month - 1);
    date.setDate(1);

    return date.getDay();
  }
  getLastWeek(year, month) {
    //当月最后一天星期几
    let date = new Date();
    date.setYear(year);
    date.setMonth(month);
    date.setDate(0);

    return date.getDay();
  }
  // 获取农历信息
  getLunarInfo(y, m, d) {
    const { curYear, curMonth, curDay } = this.state;
    let lunarInfo = calendar.solar2lunar(y, m, d);
    let lunarValue = "";
    if (this.state.lunar) {
      lunarValue = lunarInfo.IDayCn;
    }
    // console.log(lunarInfo)
    let isLunarFestival = false;
    let isGregorianFestival = false;
    let istody = false;
    if (
      this.festival.lunar[lunarInfo.lMonth + "-" + lunarInfo.lDay] != undefined
    ) {
      lunarValue = this.festival.lunar[lunarInfo.lMonth + "-" + lunarInfo.lDay];
      isLunarFestival = true;
    } else if (this.festival.gregorian[m + "-" + d] != undefined) {
      lunarValue = this.festival.gregorian[m + "-" + d];
      isGregorianFestival = true;
    } else if (
      curYear === parseInt(y) &&
      curMonth + 1 === parseInt(m) &&
      curDay === parseInt(d)
    ) {
      lunarValue = "今天";
      istody = true;
    }
    return {
      lunar: lunarValue,
      isLunarFestival: isLunarFestival,
      isGregorianFestival: isGregorianFestival,
      istody: istody
    };
  }
  gotisJI(date) {
    return new Promise((resolve, reject) => {
      http("/app/calendar.do", {
        date: date
      })
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          Taro.showToast({
            title: error.errMsg,
            icon: "none"
          });
        });
    });
  }
  showInfoLayer() {
    // 弹出明细
    this.setState({
      isshowinfo: true
    });
  }
  hideInfoLayer() {
    // 关闭明细
    this.setState({
      isshowinfo: false
    });
  }
  render() {
    const { calendars, weeks, isshowinfo, isshow } = this.state;
    // console.log(calendars);
    return (
      <View className="page date-page">
        <ScrollView
          className="content-wrapper"
          style={{ height: getWindowHeight(false, 0) }}
        >
          <View className="date-page-wrapper">
            <View className="date-title-box">
              <View className="date-title">
                <Text>11</Text>
                <Text>月</Text>
                <View className="title-year">2019 已亥年</View>
              </View>
            </View>
            <View className="date-content">
              <View className="week-box">
                {weeks.map(item => (
                  <View className="week-li" key={item.id}>
                    {item.name}
                  </View>
                ))}
              </View>
              <View className="date-ul-box">
                <View className="date-ul">
                  {calendars.map((calendarvalue, index) => (
                    <View className="date-ul-item" key={index}>
                      {calendarvalue.days &&
                        calendarvalue.days.map((day, dayindex) => (
                          <View
                            className={`date-li ${
                              day.disabled ? "date-li-disabled" : ""
                            }`}
                            key={dayindex}
                          >
                            <View className="date-li-solar">{day.value}</View>
                            <View className="date-li-lunar">{day.text}</View>
                            {
                              day.isji && ( <View className="date-li-ji">吉</View>)
                            }
                          </View>
                        ))}
                    </View>
                  ))}
                </View>
              </View>
            </View>
          </View>
          {/* 日历信息 */}
          <View
            className={`layer-info-wrapper ${
              isshowinfo ? "layer-info-active" : ""
            }`}
          >
            <Image
              src={closeicon}
              className="close-icon"
              onClick={this.hideInfoLayer.bind(this)}
            />
            <View className="info-header">
              2019年10月27号 十一月(大)初二 星期三
            </View>
            <View className="info-content">
              <View className="info-content-li">
                <View className="content-li-left">宜</View>
                <View className="content-li-right">
                  祭祀 数计 几句 数据 谁都 都是 都是 大家 大家 空间 看看 接受
                </View>
              </View>
              <View className="info-content-li">
                <View className="content-li-left">忌</View>
                <View className="content-li-right">
                  祭祀 数计 几句 数据 谁都 都是 都是 大家 大家 空间 看看 接受
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        {/* 日历 */}
        {/* <View className={`layer-wrapper ${isshow ? "layer-show" : ""}`}>
          <View
            className={`layer-mask ${isshow ? "layer-mask-active" : ""}`}
            onClick={this.hideInfoLayer.bind(this)}
          ></View>
          <View className={`layer-main ${isshow ? "layer-main-active" : ""}`}>
            <Image
              src={closeicon}
              className="close-icon"
              onClick={this.hideInfoLayer.bind(this)}
            />
            <View className="info-header"></View>
            <View className="info-content"></View>
          </View>
        </View> */}
      </View>
    );
  }
}

export default DatePage;
