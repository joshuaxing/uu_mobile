import Taro, { Component, connectSocket } from "@tarojs/taro";
import { View, Image, Picker} from "@tarojs/components";
import { observer, inject } from "@tarojs/mobx";
import { formatDate, formatWeek } from "../../utils/hanldeDate.js";
import calendar from "../../utils/calendar.js";
import { getWindowHeight } from "../../utils/style.js";
import http from "../../http/main.js";
import "./index.scss";
import closeicon from "./close-icon.png";
import arrowdown from "./arrow-down.png";
@inject("counterStore")
@observer
class DatePage extends Component {
  config = {
    navigationBarTitleText: "黄道吉日"
  };
  festival = {
    lunar: {
      "1-1": "春节",
      "1-15": "元宵节",
      "2-2": "龙头节",
      "5-5": "端午节",
      "7-7": "七夕节",
      "7-15": "中元节",
      "8-15": "中秋节",
      "9-9": "重阳节",
      "10-1": "寒衣节",
      "10-15": "下元节",
      "12-8": "腊八节",
      "12-23": "祭灶节"
    },
    gregorian: {
      "1-1": "元旦",
      "2-14": "情人节",
      "3-8": "妇女节",
      "3-12": "植树节",
      "4-5": "清明节",
      "5-1": "劳动节",
      "5-4": "青年节",
      "6-1": "儿童节",
      "7-1": "建党节",
      "8-1": "建军节",
      "9-10": "教师节",
      "10-1": "国庆节",
      "12-24": "平安夜",
      "12-25": "圣诞节"
    }
  };
  allcount = 42;
  state = {
    weeks: [
      {
        id: 0,
        name: "日"
      },
      {
        id: 1,
        name: "一"
      },
      {
        id: 2,
        name: "二"
      },
      {
        id: 3,
        name: "三"
      },
      {
        id: 4,
        name: "四"
      },
      {
        id: 5,
        name: "五"
      },
      {
        id: 6,
        name: "六"
      }
    ],
    lunar: true,
    nowYear: "",
    nowMonth: "",
    nowDay: "",
    calendars: [],
    yearCount: 100,
    isshowinfo: false,
    isshow: false,
    clickindex: -1,
    clickji: '',
    clickyi: '',
    clickyear: '',
    clickweek: '',
    clickyinli: '',
    titleyearmonth: ''
  };
  componentWillMount() {
    this.init();
  }

  componentWillReact() {
    // console.log('componentWillReact')
  }

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  calendarFactory(year, month) {
    
    let daysCount = this.getDays(year, month); //当前月天数
    let startWeek = this.getWeek(year, month); //当月第一天星期几

    //补充前一个月
    let prevYear = year;
    let prevMonth = month - 1;
    if (prevMonth <= 0) {
      prevMonth = 12
      prevYear -= 1
    }
    let prevmonthCount = this.getDays(prevYear, prevMonth); //前一个天数
    let prevdaysAll = Array.from(new Array(prevmonthCount), (val, index) => {
      return index + 1;
    });
    let prevdaysArr = prevdaysAll.filter(item => {
      if (item > prevmonthCount - startWeek) {
        return item;
      }
    });
    let prevdays = prevdaysArr.map((item, index) => {
      let lunarInfo = this.getLunarInfo(prevYear, prevMonth, item);
      let disabled = true;
      return {
        value: item,
        week: new Date(`${prevYear}/${prevMonth}/${item}`).getDay(),
        timestamp: new Date(`${prevYear}/${prevMonth}/${item}`).getTime(),
        text: lunarInfo.lunar,
        disabled: disabled,
        isLunarFestival: lunarInfo.isLunarFestival,
        isGregorianFestival: lunarInfo.isGregorianFestival,
        istody: lunarInfo.istody,
        date: `${prevYear}-${prevMonth}-${item}`,
        datestr: `${prevYear}年${prevMonth}月${item}日`
      };
    });
    prevdays.map((item, index) => {
      this.gotisJI(item.date).then(result => {
        let isji = false;
        let ji = "";
        let yi = "";
        let yinli = ""
        if (result.status === 1) {
          const data = result.data;
          data.yi.indexOf("嫁娶") !== -1 && (isji = true);
          ji = data.ji;
          yi = data.yi;
          yinli = data.yinli;
        }
        item.isji = isji;
        item.ji = ji;
        item.yi = yi;
        item.yinli = yinli
      });
    });
    //console.log(prevdays)

    //补充后一个月
    let nextYear = year;
    let nextMonth = month + 1;
    let isend = false
    if (nextMonth > 12) {
      nextMonth = 1;
      nextYear += 1;
      if (nextYear > 2015) {
        isend = true
      }
    }
    const nextmonthCount = this.allcount - daysCount - startWeek;

    let nextdaysArr = Array.from(new Array(nextmonthCount), (val, index) => {
      return index + 1;
    });

    let nextdays = nextdaysArr.map((item, index) => {
      let lunarInfo = this.getLunarInfo(nextYear, nextMonth, item);
      let disabled = true;
      return {
        value: item,
        week: new Date(`${nextYear}/${nextMonth}/${item}`).getDay(),
        timestamp: new Date(`${nextYear}/${nextMonth}/${item}`).getTime(),
        text: lunarInfo.lunar,
        disabled: disabled,
        isLunarFestival: lunarInfo.isLunarFestival,
        isGregorianFestival: lunarInfo.isGregorianFestival,
        istody: lunarInfo.istody,
        date: `${nextYear}-${nextMonth}-${item}`,
        datestr: `${nextYear}年${nextMonth}月${item}日`,
        isend: isend
      };
    });
    if (!isend) {
      nextdays.map((item, index) => {
        this.gotisJI(item.date).then(result => {
          let isji = false;
          let ji = "";
          let yi = "";
          let yinli = ""
          if (result.status === 1) {
            const data = result.data;
            data.yi.indexOf("嫁娶") !== -1 && (isji = true);
            ji = data.ji;
            yi = data.yi;
            yinli = data.yinli;
          }
          item.isji = isji;
          item.ji = ji;
          item.yi = yi;
          item.yinli = yinli;
        });
      });
    }

    // console.log(nextdays)

    //当前月
    const { nowYear, nowMonth, nowDay } = this.state;
    let currentdays = Array.from(new Array(daysCount), (val, index) => {
      let lunarInfo = this.getLunarInfo(year, month, index + 1);
      let disabled = false;
      year <= nowYear &&
        month <= nowMonth &&
        index + 1 < nowDay &&
        (disabled = true);
      return {
        value: index + 1,
        week: new Date(`${year}/${month}/${index + 1}`).getDay(),
        timestamp: new Date(`${year}/${month}/${index + 1}`).getTime(),
        text: lunarInfo.lunar,
        disabled: disabled,
        isLunarFestival: lunarInfo.isLunarFestival,
        isGregorianFestival: lunarInfo.isGregorianFestival,
        istody: lunarInfo.istody,
        date: `${year}-${month}-${index + 1}`,
        datestr: `${year}年${month}月${index + 1}日`
      };
    });
    currentdays.map((item, index) => {
      this.gotisJI(item.date).then(result => {
        let isji = false;
        let ji = "";
        let yi = "";
        let yinli = ""
        if (result.status === 1) {
          const data = result.data;
          data.yi.indexOf("嫁娶") !== -1 && (isji = true);
          ji = data.ji;
          yi = data.yi;
          yinli = data.yinli;
        }
        item.isji = isji;
        item.ji = ji;
        item.yi = yi;
        item.yinli = yinli;
      });
    });
    //console.log(currentdays);

    let days = [];
    days = prevdays.concat(currentdays, nextdays);
    return days;
  }

  createCalendars(year, month) {
    let calendars = this.calendarFactory(year, month);
    this.setState({
      calendars: calendars
    });
  }
  init() {
    let date = new Date();
    const curYear = parseInt(date.getFullYear(), 10);
    const curMonth = parseInt(date.getMonth(), 10) + 1; //月份+1
    const curDay = parseInt(date.getDate(), 10);
    this.setState({
      titleyearmonth: `${curYear}年${this.fillzero(curMonth)}月`,
      titleyearmonthvalue: `${curYear}-${this.fillzero(curMonth)}`,
      nowYear: curYear,
      nowMonth: curMonth,
      nowDay: curDay
    })
    this.createCalendars(curYear, curMonth)
  }
  gotDaysCount(year, month) {
    //获取一个月有多少天
    const isLeapYear =
      year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0) ? 1 : 0;
    const days = month === 2 ? 28 + isLeapYear : 31 - (((month - 1) % 7) % 2);
    return days;
  }
  getDays(year, month) {
    //获取一个月多少天
    let day = new Date(year, month, 0);
    return day.getDate();
  }

  getWeek(year, month) {
    //当月第一天星期几
    let date = new Date();
    date.setYear(year);
    date.setMonth(month - 1);
    date.setDate(1);

    return date.getDay();
  }
  getLastWeek(year, month) {
    //当月最后一天星期几
    let date = new Date();
    date.setYear(year);
    date.setMonth(month);
    date.setDate(0);

    return date.getDay();
  }
  // 获取农历信息
  getLunarInfo(y, m, d) {
    const { nowYear, nowMonth, nowDay } = this.state;
    let lunarInfo = calendar.solar2lunar(y, m, d);
    let lunarValue = "";
    if (this.state.lunar) {
      lunarValue = lunarInfo.IDayCn;
    }
    // console.log(lunarInfo)
    let isLunarFestival = false;
    let isGregorianFestival = false;
    let istody = false;
    if (
      this.festival.lunar[lunarInfo.lMonth + "-" + lunarInfo.lDay] != undefined
    ) {
      lunarValue = this.festival.lunar[lunarInfo.lMonth + "-" + lunarInfo.lDay];
      isLunarFestival = true;
    } else if (this.festival.gregorian[m + "-" + d] != undefined) {
      lunarValue = this.festival.gregorian[m + "-" + d];
      isGregorianFestival = true;
    } else if (
      nowYear === parseInt(y) &&
      nowMonth + 1 === parseInt(m) &&
      nowDay === parseInt(d)
    ) {
      lunarValue = "今天";
      istody = true;
    }
    return {
      lunar: lunarValue,
      isLunarFestival: isLunarFestival,
      isGregorianFestival: isGregorianFestival,
      istody: istody
    };
  }
  gotisJI(date) {
    return new Promise((resolve, reject) => {
      http("/app/calendar.do", {
        date: date
      })
        .then(result => {
          resolve(result);
          this.setState({
            isshowinfo: false,
            clickindex: -1
          });
        })
        .catch(error => {
          Taro.showToast({
            title: error.errMsg,
            icon: "none"
          });
        });
    });
  }
  showInfoLayer(item, clickindex) {
    // 显示
    if (item.isend) {
      return
    }
    const weeks = this.state.weeks;
    let clickweekarr = weeks.filter((value) => (
      value.id === item.week
    ))
    this.setState({
      isshowinfo: true,
      clickindex: clickindex,
      clickji: item.ji,
      clickyi: item.yi,
      clickyinli: item.yinli,
      clickyear: item.datestr,
      clickweek: clickweekarr[0].name
    });
  }
  hideInfoLayer() {
    // 关闭
    this.setState({
      isshowinfo: false,
      clickindex: -1
    });
  }
  bindDateChange (e) {
    //console.log(e.detail.value)
    const datevalue = e.detail.value
    if (this.state.titleyearmonthvalue !== datevalue) {
      const datearr = datevalue.split('-');
      this.createCalendars(Number(datearr[0]), Number(datearr[1]));
      this.setState({
        titleyearmonth: `${datearr[0]}年${this.fillzero(parseInt(datearr[1]))}月`,
        titleyearmonthvalue: `${datearr[0]}-${this.fillzero(parseInt(datearr[1]))}`
      })
    }
  }
  fillzero (n) {
    return n < 10 ? '0' + n : n
  }
  render() {
    const { titleyearmonth, nowYear, nowMonth, calendars, weeks, isshowinfo, clickindex, clickji, clickyi, clickyinli, clickweek, clickyear} = this.state;
    //console.log(calendars);
    return (
      <View className="page date-page">
        <View className="content-wrapper">
          <View className="date-page-wrapper">
            <View className="date-title-box">
              <Picker mode="date" onChange={this.bindDateChange.bind(this)} fields="month" start={`${nowYear}-${nowMonth}`} end="2025-12">
                <View className="date-title">{titleyearmonth}<Image src={arrowdown} className="arrow-down"/></View>
              </Picker>
            </View>
            <View className="date-content">
              <View className="week-box">
                {weeks.map(item => (
                  <View className="week-li" key={item.id}>
                    {item.name}
                  </View>
                ))}
              </View>
              <View className="date-ul-box">
                <View className="date-ul">
                  <View className="date-ul-item">
                    {calendars.length > 0 &&
                      calendars.map((day, dayindex) => (
                        <View
                          className={`date-li ${
                            day.disabled ? "date-li-disabled" : ""
                          } ${day.isji ? "date-li-jq" : ""} ${dayindex === clickindex ? 'date-li-selected' : ''}`}
                          key={dayindex}
                          onClick={this.showInfoLayer.bind(this, day, dayindex)}
                        >
                          <View className={`date-li-solar `}>{day.value}</View>
                          <View className="date-li-lunar">{day.text}</View>
                          <View className="date-li-ji">吉</View>
                        </View>
                      ))}
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        {/* 日历信息 */}
        <View className={`layer-info-wrapper ${isshowinfo ? 'layer-info-active' : ''}`}>
          <Image
            src={closeicon}
            className="close-icon"
            onClick={this.hideInfoLayer.bind(this)}
          />
          <View className="info-header">
            {clickyear} {clickyinli} 星期{clickweek}
          </View>
          <View className="info-content">
            <View className="info-content-li">
              <View className="content-li-left">宜</View>
              <View className="content-li-right">
                {clickyi}
              </View>
            </View>
            <View className="info-content-li">
              <View className="content-li-left content-li-ji">忌</View>
              <View className="content-li-right">
                {clickji}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default DatePage;
