/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-29 15:35:24
 * @LastEditTime: 2019-08-20 19:28:17
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text, Video} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class caseDetail extends Component {

  config = {
    navigationBarTitleText: '案例详情'
  }
  state = {
    result: {},
    detailid: 0,
    bannerlist: []
  }
  componentWillMount () {
    const id = this.$router.params.id;
    this.setState({
      detailid: id
    }, () => {
      this.gotData()
    })
  }

  componentWillReact () {
    // console.log('componentWillReact')
  }

  componentDidMount () { }

  componentWillUnmount () {

  }

  componentDidShow () { }

  componentDidHide () { }
  gotData () {
    http('/app/demoinfo.do', {
      id: this.state.detailid
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data;

        const value = {
          ...data,
          titleimage: data.pcpath.shift(),
          time: this.handleDate(data.demotime)
        }
        this.setState({
          result: value,
          bannerlist: data.pcpath
        })
      }
    })
  }
  handleDate (str) {
    const arr = str.split('-');
    return arr[0] + '年' + arr[1] + '月' + arr[2] + '日'
  }
  prevImage (item) {
    const { bannerlist } = this.state;
    Taro.previewImage({
      current: item,
      urls: bannerlist
    })
  }
  render () {
    const { result, bannerlist} = this.state;
    return (
      <View className="page casedetail-page">
        <View className="top-banner">
          <Image src={result.titleimage} className="banner-image"/>
        </View>
        <View className="header">
          <View className="label-name">
            <View className="name">{result.name}</View>
            {
              result.labelid === 4 &&
              (<View className="label">标准</View>)
            }
            {
              result.labelid === 5 &&
              (<View className="label">高端</View>)
            }
          </View>
          <View className="info">
            <Text className="info-type">{result.regionname}</Text>
            <Text className="info-line">|</Text>
            <Text className="info-table">{result.address}</Text>
          </View>
          <View className="time">{result.time}</View>
          <View className="des">{result.introduction}</View>
        </View>
        <View className="video-box">
          <Video
            src={result.demomv}
            controls={true}
            autoplay={false}
            poster = "https://www.uuwed.com/miniprogram/video-case.jpg"
            initialTime="0.1"
            id="video"
            loop={false}
            muted={false}
            className="case-video"
          />
        </View>
        <View className="case-images">
          {
            bannerlist.map((item, index) => (
              <View className="case-image-item" key={item}>
                <Image src= {item} className="case-image" lazyLoad='true' onClick={this.prevImage.bind(this, item)} />
              </View>
            ))
          }
        </View>
      </View>
    )
  }
}

export default caseDetail
