import Taro, { Component } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('counterStore')
@observer
class SwiperBanner extends Component {

  static defaultProps = {
    list: []
  }
  componentWillMount () { }

  componentWillReact () {
    
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }


  render () {
    const { list } = this.props
    return (
      <View className='hoteldetail-banner'>
        <Swiper
          className='hoteldetail-banner__swiper'
          circular
          autoplay
          indicatorDots = {true}
          indicatorColor = 'rgb(255,255,255,1)'
          indicatorActiveColor='rgb(252,71,71)'
          // TODO 目前 H5、RN 暂不支持 previousMargin、nextMargin
          // previousMargin
          // nextMargin
        >
          {list.map((item, index)=> (
            <SwiperItem
              key={index}
              className='hoteldetail-banner__swiper-item'
            >
              <Image
                className='hoteldetail-banner__swiper-item-img'
                src={item}
              />
              <View className="label"></View>
            </SwiperItem>
          ))}
        </Swiper>
      </View>
    )
  }
}

export default SwiperBanner
