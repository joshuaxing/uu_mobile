/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-18 15:22:16
 * @LastEditTime: 2019-08-25 23:50:27
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { AtIndexes, AtIcon } from 'taro-ui'
import './index.scss'
import getFirstLetter from '../../utils/pinyin/index.js'
import http from '../../http/main.js'
import adIcon from './ad_icon.png'
import arrowsDown from './arrows_down.png'
@inject('counterStore')
@observer
class City extends Component {

  config = {
    navigationBarTitleText: '选择城市'
  }
  state = {
    citylist: [],
    hotCitys: [],
    isdown: false,
    areaList: []
  }
  gotCity () {
    http('/app/addresslist.do')
    .then((result) => {
      if (result.status === 1) {
        let citys = result.data.address;
        let hotCitys = result.data.city;
        let citykeys = {}
        citys.map((item, index) => {
          let key = getFirstLetter(item.name.split('')[0])
          item.key = key;
          citykeys[key] = [];
          return item
        })
        citys.sort((a, b) => {
          return a.key.localeCompare(b.key)
        })
        citys.map((item, index) => {
          citykeys[item.key].push(item)
        })
        let citylist = [];
        for (let key in citykeys) {
          let value = {
            title: key,
            key: key,
            items: citykeys[key]
          }
          citylist.push(value)
        }
        this.setState({
          citylist: citylist,
          hotCitys: hotCitys
        })
      }
    })
  }
  gotArea () {
    const { currentcity } = this.props.counterStore
    http('/areas/selectbyparentid.do', {
      parentid: currentcity.id
    })
    .then((result) => {
      if (result.status === 1) {
        let areaList = result.data;
        areaList.unshift({
          areaname: '全城',
          areaid: 0
        })
        this.setState({
          areaList: areaList
        })
      }
    })
  }
  clickCity(item) {
    const { counterStore } = this.props
    let value = {
      areaname: '全城',
      areaid: 0,
      name: item.name,
      id: item.id
    }
    // console.log(value)
    counterStore.gotCurrentCity(value)
    Taro.setStorageSync('_currentcity', JSON.stringify(value))
    let pageIndex = 0;
    if (process.env.TARO_ENV === 'h5') {
      pageIndex = window.history.length
    } else {
      pageIndex = Taro.getCurrentPages().length;
    }
    if (pageIndex === 1) {
      Taro.switchTab({
        url: '/pages/index/index'
      })
    } else {
      Taro.navigateBack({
        delta: 1
      })
    }
  }
  handleArea () {
    const { isdown } = this.state
    this.setState({
      isdown: !isdown
    })
  }
  clickArea (data, e) {
    const { counterStore } = this.props;
    let value = {
      areaname: data.areaname,
      areaid: data.areaid,
      name: counterStore.currentcity.name,
      id: counterStore.currentcity.id
    }
    // console.log(value)
    counterStore.gotCurrentCity(value)
    Taro.setStorageSync('_currentcity', JSON.stringify(value))
    this.setState({
      isdown: false
    })
    let pageIndex = 0;
    if (process.env.TARO_ENV === 'h5') {
      pageIndex = window.history.length
    } else {
      pageIndex = Taro.getCurrentPages().length;
    }
    if (pageIndex === 1) {
      Taro.switchTab({
        url: '/pages/index/index'
      })
    } else {
      Taro.navigateBack({
        delta: 1
      })
    }
  }
  componentWillMount () {
    this.gotCity();
    this.gotArea();
  }

  componentWillReact () {
    // console.log('componentWillReact')
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }


  render () {
    const { currentcity } = this.props.counterStore
    const { citylist, hotCitys, areaList, isdown} = this.state;
    return (
      <View className="page city-page">

       <AtIndexes
          list={citylist}
          onClick={this.clickCity.bind(this)}
          topKey = "城市定位"
          isShowToast = {false}
          isVibrate = {false}
          animation = {true}
        >
          <View className="area-box">
            <View className="area-title">
              <View className="area-title-left">
                当前：{currentcity.name}{currentcity.areaname}
              </View>
              <View
                className={isdown ? 'area-title-right title-right-active' : 'area-title-right'}
                onClick={this.handleArea.bind(this)}

              >
                <Text className="right-txt">切换区县</Text>
                <Image src={arrowsDown} className="right-icon"/>
              </View>
            </View>
            {
              isdown &&  (<View className="area-ul">
                {
                  areaList.map((item, index) => {
                    return (<View
                      className={item.areaid === currentcity.areaid ? 'area-li-active area-li' : 'area-li'}
                      key={item.areaid}
                      onClick={(e) => this.clickArea(item, e)}
                    >
                      {item.areaname}
                    </View>)
                  })
                }
              </View>)
            }
          </View>
          <View className="citys-header">
            <View className = "citys-other position-citys" >
              <View className="name">默认城市:</View>
              <View className="city-ul">
                <View className="city-li"  onClick={(e) => this.clickCity({name: '上海',id: 793}, e)}>
                  <Image src={adIcon} className="ad-icon"/>
                  <Text className="ad-name">上海市</Text>
                </View>
              </View>
            </View>
            <View className="citys-other hot-citys">
              <View className="name">热门城市:</View>
              <View className="city-ul">
                {
                  hotCitys.map((item, index) => {
                    return (<View
                      className="city-li" key={item.id}
                      onClick={(e) => this.clickCity(item, e)}
                    >
                      <Text className="ad-name">{item.name}</Text>
                    </View>)
                  })
                }
              </View>
            </View>
          </View>
        </AtIndexes>
      </View>
    )
  }
}

export default City
