/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-18 16:01:01
 * @LastEditTime: 2019-08-22 10:13:27
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Image, ScrollView} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import { getWindowHeight } from '../../utils/style.js'
import './index.scss'
import activityIcon from './me-activity.png'
import collectIcon from './me-collect.png'
import orderIcon from './me-order.png'
import tiezi from './tiezi.png'
import arrow from './arrow.png'
import jusuanqi from './jusuanqi.png'
import http from '../../http/main.js'
@inject('counterStore')
@observer
class MyPage extends Component {

  config = {
    navigationBarTitleText: '我的',
    enablePullDownRefresh: true
  }
  state = {
    name: '',
    phone: '',
    collectionnum: 0,
    activitynum: 0,
    subscribenum: 0,
    photo: ''
  }
  componentWillMount () {
    const { counterStore: { user } } = this.props
    if (user.id) {
      this.gotData()
    }
  }

  componentWillReact () {
    const { counterStore: { user } } = this.props
    if (user.id) {
      this.gotData()
    }
  }

  componentDidMount () {
    const { counterStore: { user } } = this.props
    if (user.id) {
      this.gotData()
    }
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  onPullDownRefresh() {
    //下拉刷新
    const { counterStore: { user } } = this.props
    if (user.id) {
      this.gotData()
    } else {
      Taro.stopPullDownRefresh();
    }
  }

  gotData = () => {
    const { counterStore: { user } } = this.props
    //获取用户信息
    http('/app/userinfo.do', {
      userid: user.id
    })
    .then((result) => {
      if (result.status === 1) {
        this.setState({
          name: result.data.name,
          phone: result.data.account,
          collectionnum: result.data.collectionnum,
          activitynum: result.data.activitynum,
          subscribenum: result.data.subscribenum,
          photo: result.data.pc
        })
      }
      Taro.stopPullDownRefresh();
    })
  }
  goLogin = () => {
    //去登录
    Taro.navigateTo({
      url: '/pages/login/index'
    })
  }
  goLogout = () => {
    //退出
    const { counterStore } = this.props;
    const value = {
      account: '',
      id: 0
    }
    counterStore.gotUserInfo(value)
    Taro.removeStorageSync('uu_user2030')
    Taro.navigateTo({
      url: '/pages/login/index'
    })
  }
  handleSubscribe() {
    const { counterStore: { user } } = this.props
    if (user.id) {
      Taro.navigateTo({
        url: '/pages/orderlist/index'
      })
    } else {
      Taro.navigateTo({
        url: '/pages/login/index'
      })
    }
  }
  handleActivity() {
    const { counterStore: { user } } = this.props
    if (user.id) {
      Taro.showToast({
        title: '正在筹划中，敬请期待',
        icon: 'none',
        duration: 2000
      })
    } else {
      Taro.navigateTo({
        url: '/pages/login/index'
      })
    }
  }
  handleCollection() {
    const { counterStore: { user } } = this.props
    if (user.id) {
     Taro.showToast({
       title: '正在完善中，敬请期待',
       icon: 'none',
       duration: 2000
     })
    } else {
      Taro.navigateTo({
        url: '/pages/login/index'
      })
    }
  }
  goComputed () {
    Taro.navigateTo({
      url: '/pages/jsqpage/index'
    })
  }
  goCard () {
    Taro.navigateTo({
      url: '/pages/cardlist/index'
    })
  }
  handleMyCard () {
    Taro.navigateTo({
      url: '/pages/mycard/index'
    })
  }
  goDate () {
    Taro.navigateTo({
      url: '/pages/date/index'
    })
  }
  render () {
    const { counterStore: { user } } = this.props;
    const { name, phone, activitynum, collectionnum, subscribenum, photo} = this.state;
    return (
      <View className="page me-page">
        {
          user.id ? (<View className="admin-head">
            <View className="admin-image">
              <Image src={photo} className="user-image"/>
            </View>
            <View className="admin-content">
              <View className="name">{name}</View>
              <View className="phone">{phone}</View>
            </View>
          </View>) : (<View className="admin-head">
            <View className="admin-txt" onClick={this.goLogin}>请先登录或者注册</View>
        </View>)
        }

        <ScrollView
          className="content-wrapper"
          scrollY
          style={{ height: getWindowHeight(true, 90)}}
        >
          <View className="table-list-box">
            <View className="table-list" onClick={this.handleSubscribe.bind(this)}>
              <Image src={orderIcon} className="list-image"/>
              <View className="count">{subscribenum}</View>
              <View className="name">我的预约</View>
            </View>
            <View className="table-list" onClick={this.handleActivity.bind(this)}>
              <Image src={activityIcon} className="list-image"/>
              <View className="count">{activitynum}</View>
              <View className="name">我的活动</View>
            </View>
            <View className="table-list" onClick={this.handleCollection.bind(this)}>
              <Image src={collectIcon} className="list-image"/>
              <View className="count">{collectionnum}</View>
              <View className="name">我的收藏</View>
            </View>
            {/* <View className="table-list" onClick={this.handleMyCard.bind(this)}>
              <Image src={tiezi} className="list-image"/>
              <View className="count">{collectionnum}</View>
              <View className="name">我的请帖</View>
            </View> */}
          </View>
          <View className="table-ul">
            <View className="table-li" onClick ={ this.goComputed } >
              <Image src={jusuanqi} className="table-li-icon"/>
              <View className="table-li-left">结婚预算</View>
              <Image src={arrow} className="table-li-icon"/>
            </View>
            {/* <View className="table-li" onClick ={ this.goCard } >
              <Image src={jusuanqi} className="table-li-icon"/>
              <View className="table-li-left">电子请柬</View>
              <Image src={arrow} className="table-li-icon"/>
            </View> */}
            <View className="table-li" onClick ={ this.goDate } >
              <Image src={jusuanqi} className="table-li-icon"/>
              <View className="table-li-left">黄道吉日</View>
              <Image src={arrow} className="table-li-icon"/>
            </View>
          </View>
          
          {
            user.id ? (<View className="back-btn" onClick ={ this.goLogout } >退出登录</View>) : null
          }
        </ScrollView>
      </View>
    )
  }
}

export default MyPage
