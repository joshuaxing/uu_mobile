/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-22 15:07:54
 * @LastEditTime: 2019-09-06 11:29:05
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import Header from '../../components/header/index.js'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'
import phoneIcon from './phone.png'
@inject('counterStore')
@observer
class Car extends Component {

  config = {
    navigationBarTitleText: '婚车',
    enablePullDownRefresh: true,
    onReachBottomDistance: 0
  }
  sortList = [
    {id: 0, name: '默认排序', prosort: '', sorttype: 0},
    {id: 1, name: '价格升序', prosort: 'moneytype', sorttype: 1},
    {id: 2, name: '价格降序', prosort: 'moneytype', sorttype: 2},
    {id: 3, name: '星级升序', prosort: 'type', sorttype: 1},
    {id: 4, name: '星级降序', prosort: 'type', sorttype: 2},
  ]
  timer = null
  state = {
    istoupper: false,
    totalpage: 0,
    page: 1,
    limit: 10,
    loading: false,
    hasMore: true,
    nodata: true,
    result: [],
    selectIndex: 0,
    selectOpen: false,
    sortform: {
      id: 0,
      prosort: '',
      sorttype: 0,
      name: '默认排序'
    },
    typeform: {
      id: 0,
      name: '车型'
    },
    moneyform: {
      id: 0,
      name: '价格'
    },
    brandform: {
      id: 0,
      name: '品牌'
    },
    typeList: [],
    pirceList: [],
    brandList: []
  }

  componentWillMount () {
    this.gotData();
    this.gotSelectData();
  }

  componentWillReact () {
    this.gotSelectData();
    this.setState({
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  componentDidMount () {

  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  onPullDownRefresh() {
    //下拉刷新
    this.gotSelectData();
    this.setState({
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  searchMoney (item) {
    if (item.id) {
      this.setState({
        moneyform: item,
        selectIndex: 0,
        selectOpen: false,
        result: [],
        page: 1,
        loading: false,
        hasMore: true,
        nodata: true
      }, () => {
        this.gotData();
      })
    } else {
      this.setState({
        moneyform: {
          id: 0,
          name: '价格'
        },
        selectIndex: 0,
        selectOpen: false,
        result: [],
        page: 1,
        loading: false,
        hasMore: true,
        nodata: true
      }, () => {
        this.gotData();
      })
    }
  }
  searchType(item) {
    if (item.id) {
      this.setState({
        typeform: item,
        selectIndex: 0,
        selectOpen: false,
        result: [],
        page: 1,
        loading: false,
        hasMore: true,
        nodata: true
      }, () => {
        this.gotData();
      })
    } else {
      this.setState({
        typeform: {
          id: 0,
          name: '车型'
        },
        selectIndex: 0,
        selectOpen: false,
        result: [],
        page: 1,
        loading: false,
        hasMore: true,
        nodata: true
      }, () => {
        this.gotData();
      })
    }
  }
  searchBrand(item) {
    if (item.id) {
      this.setState({
        brandform: item,
        selectIndex: 0,
        selectOpen: false,
        result: [],
        page: 1,
        loading: false,
        hasMore: true,
        nodata: true
      }, () => {
        this.gotData();
      })
    } else {
      this.setState({
        brandform: {
          id: 0,
          name: '品牌'
        },
        selectIndex: 0,
        selectOpen: false,
        result: [],
        page: 1,
        loading: false,
        hasMore: true,
        nodata: true
      }, () => {
        this.gotData();
      })
    }
  }
  colseSelect () {
    this.setState({
      selectIndex: 0,
      selectOpen: false
    })
  }
  handleSort(index) {
    const { selectOpen, selectIndex } = this.state;
    if (selectIndex !== index && selectOpen) {
      this.setState({
        selectIndex: index
      })
    } else {
      this.setState({
        selectIndex: index,
        selectOpen: !selectOpen
      })
    }
  }
  gotSelectData () {
    //车辆品牌
    http('/app/vehiclebrandlist.do')
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "不限"
        })
        this.setState({
          brandList: data
        })
      }
    })
    //金额
    http('/app/moneytype.do',{
      type: 5
    })
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "不限"
        })
        this.setState({
          pirceList: data
        })
      }
    })
    //车辆类型
    http('/app/vehicletype.do')
    .then((result) => {
      if (result.status === 1) {
        let data = result.data;
        data.unshift({
          id: 0,
          name: "不限"
        })
        this.setState({
          typeList: data
        })
      }
    })
  }
  gotData () {
    //列表数据
    const { counterStore: { currentcity } } = this.props
    const { page, limit, hasMore, loading, moneyform, typeform, brandform} = this.state;
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true })
    http('/app/carlistbycity.do', {
      cityid: currentcity.id,
      areaid: currentcity.areaid,
      page: page,
      limit: limit,
      color: 0,
      brandid: brandform.id,
      type: typeform.id,
      moneytype: moneyform.id
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (alldata.length) {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: true
          })
        } else {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: false
          })
        }
      }
      Taro.stopPullDownRefresh();
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
  handleDetail(item) {
    Taro.navigateTo({
      url: `/pages/cardetail/index?id=${item.id}`
    })
  }
  orderCar(item) {
    const { counterStore: { user } } = this.props
    if (!user.id) {
      Taro.navigateTo({
        url: '/pages/login/index'
      })
    } else {
      http('/app/addvehiclesubscribe.do', {
        carid: item.id,
        userid: user.id,
        phone: user.account
      })
      .then((result) => {
        Taro.showToast({
          title: result.msg,
          icon: 'none',
          duration: 2000
        })
      })
    }
  }
  scrollToupper(e) {
    if (this.state.istoupper) return false;
    this.setState({
      istoupper: true
    })
    if (this.timer) {
      clearTimeout(this.timer)
      // console.log('clearTimeout')
    }

    this.gotSelectData();
    this.setState({
      selectIndex: 0,
      selectOpen: false,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
      this.timer = setTimeout(() => {
        this.setState({
          istoupper: false
        })
        // console.log('setTimeout')
      }, 1500)
    })
    // console.log(8888)
  }
  render () {
    const {
      result,
      moneyform,
      typeform,
      brandform,
      selectIndex,
      selectOpen,
      nodata,
      brandList,
      typeList,
      pirceList
    } = this.state;
    const { counterStore: { currentcity } } = this.props;
    let selectshow = 0;
    if (selectIndex === 1 && selectOpen) {
      selectshow = 1;
    } else if (selectIndex === 2 && selectOpen) {
      selectshow = 2;
    } else if (selectIndex === 3 && selectOpen) {
      selectshow = 3;
    }
    return (
      <View className="page car-page">
        <Header cityname={currentcity.name} areaname= {currentcity.areaname}></Header>
        <View className="car-page-content">
          <View className="search-result-box">
            <View className="search-title-ul">
              <View
                onClick={this.handleSort.bind(this, 1)}
                className={selectIndex === 1 && selectOpen ? 'search-title search-title-acitve' : 'search-title'}
              >
                <Text className="title-txt">{moneyform.name}</Text>
                <View className="title-icon at-icon at-icon-chevron-down"></View>
              </View>
              <View
                onClick={this.handleSort.bind(this, 2)}
                className={selectIndex === 2 && selectOpen ? 'search-title search-title-acitve' : 'search-title'}
              >
                <View className="title-txt">{typeform.name}</View>
                <View className="title-icon at-icon at-icon-chevron-down"></View>
              </View>
              <View
                className={selectIndex === 3 && selectOpen ? 'search-title search-title-acitve' : 'search-title'}
                onClick={this.handleSort.bind(this, 3)}
              >
                <View className="title-txt">{brandform.name}</View>
                <View className="title-icon at-icon at-icon-chevron-down"></View>
              </View>
            </View>
            {/* 筛选条件 */}

            {/* 价格 */}
            {
              selectshow === 1 &&
              (<View className="search-content-ul" onClick={this.colseSelect.bind(this)}>
                  <View className="search-content-sort">
                    {
                      pirceList.map((item) => (
                        <View
                          key={item.id}
                          className={item.id === moneyform.id ? 'sort-li sort-li-acitve' : 'sort-li'}
                          onClick={this.searchMoney.bind(this, item)}
                        >
                          <View className="sort-txt">{item.name}</View>
                          <View className="sort-txt-icon at-icon at-icon-check"></View>
                        </View>
                      ))
                    }
                  </View>
                </View>
              )
            }
            {/* 车型 */}
            {
              selectshow === 2 &&
              (<View className="search-content-ul" onClick={this.colseSelect.bind(this)}>
                  <View className="search-content-sort">
                    {
                      typeList.map((item) => (
                        <View
                          key={item.id}
                          className={item.id === typeform.id ? 'sort-li sort-li-acitve' : 'sort-li'}
                          onClick={this.searchType.bind(this, item)}
                        >
                          <View className="sort-txt">{item.name}</View>
                          <View className="sort-txt-icon at-icon at-icon-check"></View>
                        </View>
                      ))
                    }
                  </View>
              </View>)
            }
            {/* 品牌 */}
            {
              selectshow === 3 &&
              (<View className="search-content-ul">
                <View className="search-content-other">
                  <View className="content-other-box">
                    <View className="other-item-ul">
                      <View className="other-item-li">
                        {
                          brandList.map((item) => (
                            <View
                              key = {item.id}
                              className={brandform.id === item.id ? 'other-item other-item-active' : 'other-item'}
                              onClick={this.searchBrand.bind(this, item)}
                            >{item.name}</View>
                          ))
                        }
                      </View>
                    </View>
                  </View>
                </View>
              </View>)
            }
          </View>
          <ScrollView
            className="content-wrapper"
            scrollY
            style={{ height: getWindowHeight(true, 80)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          >
            <View className="content-item-ul">
              {
                result.map((item) => (
                  <View className="content-item-li" key={item.id}>
                    <View className="content-item-image" onClick={this.handleDetail.bind(this, item)}>
                      <Image src={item.pcpath[0]}  className="item-image"></Image>
                    </View>
                    <View className="content-item" onClick={this.handleDetail.bind(this, item)}>
                      <View className="name">{item.name}</View>
                      <View className="car-price">
                        <View className="discount-price"><Text className="txt">￥</Text>{item.daydiscountmoney/2}</View>
                        <View className="current-price"><Text className="txt">原价:</Text>￥{item.daymoney/2}</View>
                      </View>
                      <View className="table car-table">
                        <Text>{item.halfday}</Text>
                        <Text className="line">|</Text>
                        <Text>{item.wholeday}</Text>
                      </View>
                      <View className="lable">{item.introduce}</View>
                    </View>
                    <View className="content-item-icon" onClick={this.orderCar.bind(this, item)}>
                      <View className="icon-box">
                        <Image src={phoneIcon} className="phone-icon"/>
                        <View className="btn">立即预约</View>
                      </View>
                    </View>
                  </View>
                ))
              }
            </View>
            {
              !nodata && <Nodata></Nodata>
            }
            {this.state.loading &&
              <View className='home__loading'>
                <Text className='home__loading-txt'>正在加载中...</Text>
              </View>
            }
            {!this.state.hasMore &&
              <View className='home__loading home__loading--not-more'>
                <Text className='home__loading-txt'>没有更多内容了</Text>
              </View>
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Car
