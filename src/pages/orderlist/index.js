import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image, ScrollView } from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import './index.scss'
import arrowsRight from './arrows_right.png'
import http from '../../http/main.js'
@inject('counterStore')
@observer
class OrderPage extends Component {

  config = {
    navigationBarTitleText: '我的预约'
  }
  state = {
    totalpage: 0,
    page: 1,
    limit: 10,
    nodata: true,
    loading: false,
    hasMore: true,
    result: []
  }
  componentWillMount () {
    const { counterStore: { user } } = this.props
    if (user.id) {
      this.gotData()
    }
  }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  gotData () {
    //列表数据
    const { counterStore: { user } } = this.props;
    const { page, limit, hasMore, loading} = this.state;
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true })
    http('/app/mysubscribe.do', {
      userid: user.id,
      page: page,
      limit: limit
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (alldata.length) {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: true
          })
        } else {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: false
          })
        }
      }
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
  handleClick (item) {
    const type = item.type;
    if (type === 1) {
      Taro.switchTab({
        url: '/pages/hotel/index'
      })
    } else if (type === 2) {
      Taro.navigateTo({
        url: '/pages/door/index'
      })
    } else {
      Taro.switchTab({
        url: '/pages/car/index'
      })
    }
  }
  render () {
    const {result, nodata} = this.state;
    return (
      <View className="page orderlist-page">      
        <ScrollView 
          scrollY
          style={{ height: getWindowHeight(false, 0)}}
          onScrollToLower={this.loadRecommend.bind(this)}
        >
          <View className="orderlist-box">
            {
              result.map((item, index) => (
                 <View className="order-list" key={item.id} onClick={this.handleClick.bind(this, item)}>
                  <View className="list-image">
                    <Image src={item.image} className="image"/>
                  </View>
                  <View className="list-content">
                    <View className="name">{item.name}</View>
                    <View className="content">
                      <Text className="price">{item.moneyname}</Text>
                      <Text className="area">{item.regionname}</Text>
                    </View>
                  </View>
                  <Image src={arrowsRight} className="list-icon-right"/>
                </View>
              ))
            }
          </View>
          {
            !nodata && <Nodata/>
          }
          {this.state.loading &&
            <View className='home__loading'>
              <Text className='home__loading-txt'>正在加载中...</Text>
            </View>
          }
          {!this.state.hasMore &&
            <View className='home__loading home__loading--not-more'>
              <Text className='home__loading-txt'>没有更多内容了</Text>
            </View>
          }
        </ScrollView>
      </View>
    )
  }
}

export default OrderPage
