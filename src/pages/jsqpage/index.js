/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-23 13:39:40
 * @LastEditTime: 2019-08-22 09:48:00
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image, Input} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'
let baseurl = 'https://www.uuwed.com/miniprogram'
@inject('counterStore')
@observer
class JsqPage extends Component {
  config = {
    navigationBarTitleText: '结婚预算'
  }
  state = {
    hotelprice: 0,
    videoprice: 0,
    ringprice: 0,
    candyprice: 0,
    makeupprice: 0,
    carprice: 0,
    hostprice: 0,
    clothesprice: 0,
    planprice: 0,
    photoprice: 0,
    isdisabled: true,
    total: ''
  }

  componentWillMount () {
    
  }

  componentWillReact () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  hotelTips () {
    Taro.showModal({
      title: '婚宴酒店',
      content: '一线城市大多数酒店婚宴餐标在4000元~6000元/桌，二三线城市大多数酒店婚宴餐标在1500元~3000元/桌，县城酒店的婚宴餐标在1000左右/桌。以上仅供参考，价格影响因素受地区、酒店档次、菜品均会有差异。婚宴预算=餐标*桌数。',
      showCancel: false,
      confirmText: 'OK',
      confirmColor: '#fd0311'
    })
  }
  planTips () {
    Taro.showModal({
      title: '婚礼策划',
      content: '优品婚匠策划分为主题婚礼和定制婚礼，优品婚匠大多数新人主题婚礼布置预算在10000~30000之间，可满足大部分新人需求，主题婚礼产品性价比高，而定制婚礼可根据新人情况进行个性化定制，价格高于主题婚礼。',
      showCancel: false,
      confirmText: 'OK',
      confirmColor: '#fd0311'
    })
  }
  photoTips () {
    Taro.showModal({
      title: '婚纱摄影',
      content: '一般有单机位和双机位，单机位由一位摄像师全程跟拍婚礼全过程，双机位由两位分工不同的摄像师全程跟拍婚礼全过程。双机位从不同的方位进行拍摄，价格高于单机位。',
      showCancel: false,
      confirmText: 'OK',
      confirmColor: '#fd0311'
    })
  }
  clothesTips () {
    Taro.showModal({
      title: '婚纱礼服',
      content: '可租可买，由于礼服保管不方便，大多数新人选择租赁，大众消费在2000元左右。现代婚礼也不仅仅限制于传统西装和婚纱，也可选择汉服及晚礼服出席婚礼。',
      showCancel: false,
      confirmText: 'OK',
      confirmColor: '#fd0311'
    })
  }
  ringTips () {
    Taro.showModal({
      title: '婚戒首饰',
      content: '结婚一般会购置三金或五金，三金包涵了戒指、耳环、项链，或者手镯、耳环、项链。五金包含项链、戒指、耳环、手镯、吊坠，或者项链、戒指、耳环、手链（手镯）和脚链。',
      showCancel: false,
      confirmText: 'OK',
      confirmColor: '#fd0311'
    })
  }
  handleHotelprice = (e) => {
    const value = e.detail.value;
    this.setState({
      hotelprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleVideoprice = (e) => {
    const value = e.detail.value;
    this.setState({
      videoprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleRingprice = (e) => {
    const value = e.detail.value;
    this.setState({
      ringprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleCandyprice = (e) => {
    const value = e.detail.value;
    this.setState({
      candyprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleMakeupprice = (e) => {
    const value = e.detail.value;
    this.setState({
      makeupprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleCarprice = (e) => {
    const value = e.detail.value;
    this.setState({
      carprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleHostprice = (e) => {
    const value = e.detail.value;
    this.setState({
      hostprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handleClothesprice = (e) => {
    const value = e.detail.value;
    this.setState({
      clothesprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handlePlanprice = (e) => {
    const value = e.detail.value;
    this.setState({
      planprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handlePhotoprice = (e) => {
    const value = e.detail.value;
    this.setState({
      photoprice: value
    }, () => {
      this.handleTotalComputed();
    })
  }
  handlePhotoprice = (e) => {
    const value = e.detail.value;
    this.setState({
      photoprice: value
    }, () => {
      this.handleTotalComputed();
    })
    
  }
  handleTotalComputed () {
    const total = this.state.hotelprice*1 + this.state.videoprice*1 + this.state.ringprice*1 + this.state.candyprice*1 + this.state.makeupprice*1 + this.state.carprice*1 + this.state.hostprice*1 + this.state.clothesprice*1 + this.state.planprice*1 + this.state.photoprice*1;
    this.setState({
      total: total
    })
  }
  conputedTotal = (e) => {
    const value = e.detail.value;
    this.setState({
      total: value
    })
  }
  handleSure () {
    const total = this.state.total;
    if (total < 10000) {
      Taro.showToast({
        title: '预算金额不能低于10000',
        icon: 'none',
        duration: 2000
      })
      return
    }
   
    const hotelprice = Math.round(total * 0.5) //酒店
    const planprice = Math.round(total * 0.15) //策划
    const photoprice = Math.round(total * 0.04) //婚纱照
    const clothesprice = Math.round(total * 0.02) //婚纱
    const carprice = Math.round(total * 0.04) //婚车
    const candyprice = Math.round(total * 0.055) //糖果
    const ringprice = Math.round(total * 0.15) //戒指
    const makeupprice = Math.round(total * 0.015) //化妆
    const videoprice = Math.round(total * 0.015) //摄影
    const hostprice = Math.round(total * 0.015) //司仪

    this.setState({
      hotelprice: hotelprice,
      videoprice: videoprice,
      ringprice: ringprice,
      candyprice: candyprice,
      makeupprice: makeupprice,
      carprice: carprice,
      hostprice: hostprice,
      clothesprice: clothesprice,
      planprice: planprice,
      photoprice: photoprice,
      isdisabled: false
    })
  }
  handleCancle () {
    this.setState({
      hotelprice: 0,
      videoprice: 0,
      ringprice: 0,
      candyprice: 0,
      makeupprice: 0,
      carprice: 0,
      hostprice: 0,
      clothesprice: 0,
      planprice: 0,
      photoprice: 0,
      isdisabled: true,
      total: ''
    })
  }
  goOrder () {
    Taro.navigateTo({
      url: "/pages/orderstore/index"
    });
  }
  render () {
    const { total, hotelprice, videoprice, ringprice, candyprice, makeupprice, carprice, hostprice, clothesprice, planprice, photoprice, isdisabled} = this.state;
    return (
      <View className="page jsq-page">
        <ScrollView
          className="content-wrapper"
          scrollY
          style={{ height: getWindowHeight(true, 0)}}
        >
          <View className="jsq-page-wrapper">
            <Image src={`${baseurl}/top.jpg`} className="bg-image"/>
            <View className="jsq-page-content">
              <View className="compute-box">
                <View className="title">为你分配最合理的婚礼预算</View>
                <View className="input-box">
                  <Input type="number" placeholder="请输入您的预算金额" value={total} className="input-item" disabled={!isdisabled} onInput={this.conputedTotal}/>
                </View>
                {
                  isdisabled ? 
                    (<View className="compute-btn" onClick={this.handleSure.bind(this)}>开始计算</View>) : (<View className="compute-btn compute-btn-cancle" onClick={this.handleCancle.bind(this)}>重置计算</View>)
                }
                
                <View className="compute-tips">全国80%的新人婚礼预算在5-50万之间</View>
              </View>
              <View className="compute-table">
                <View className="compute-table-title">建议分配预算</View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-hotel.png`} className="item-image"/>
                  
                  <View className="item-content">
                    <View className="name-box" onClick={this.hotelTips}>
                      <View className="name">婚宴酒店</View>
                      <Image src={`${baseurl}/tips-icon.png`} className="name-image"/>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={hotelprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleHotelprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-plan.png`}  className="item-image"/>
                  <View className="item-content">
                    <View className="name-box" onClick={this.planTips}>
                      <View className="name">婚礼策划</View>
                      <Image src={`${baseurl}/tips-icon.png`} className="name-image"/>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input type="number" value={planprice} className="price-input" disabled={isdisabled} onInput={this.handlePlanprice}/>
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-photo.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box" onClick={this.photoTips}>
                      <View className="name">婚纱摄影</View>
                      <Image src={`${baseurl}/tips-icon.png`} className="name-image"/>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={photoprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handlePhotoprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-clothes.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box" onClick={this.clothesTips}>
                      <View className="name">婚纱礼服</View>
                      <Image src={`${baseurl}/tips-icon.png`} className="name-image"/>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={clothesprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleClothesprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-car.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box">
                      <View className="name">婚车租赁</View>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={carprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleCarprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-host.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box">
                      <View className="name">婚礼主持</View>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={hostprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleHostprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-makeup.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box">
                      <View className="name">婚礼跟妆</View>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={makeupprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleMakeupprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-video.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box">
                      <View className="name">摄影摄像</View>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={videoprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleVideoprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-candy.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box">
                      <View className="name">烟酒喜糖</View>
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={candyprice} 
                        className="price-input" 
                        disabled={isdisabled} 
                        onInput={this.handleCandyprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
                <View className="compute-table-item">
                  <Image src={`${baseurl}/jisuan-ring.png`} className="item-image"/>
                  <View className="item-content">
                    <View className="name-box" onClick={this.ringTips}>
                      <View className="name">婚戒首饰</View>
                      <Image src={`${baseurl}/tips-icon.png`} className="name-image" />
                    </View>
                    <View className="price-box">
                      <View className="price-icon">￥</View>
                      <Input 
                        type="number" 
                        value={ringprice} 
                        className="price-input" 
                        disabled={isdisabled}
                        onInput={this.handleRingprice}
                      />
                      {
                        !isdisabled ? <Image src={`${baseurl}/edit.png`} className="edit-icon"/> : null
                      }
                    </View>
                  </View>
                </View>
              </View>
              <View className="order-btn-box">
                <View className="order-btn" onClick={this.goOrder}>我要获取最优方案</View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default JsqPage
