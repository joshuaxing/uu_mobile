import Taro, { Component } from '@tarojs/taro'
import { View, Map } from '@tarojs/components'
import { getWindowHeight } from '../../utils/style.js'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'

@inject('counterStore')
@observer
class MapPage extends Component {

  config = {
    navigationBarTitleText: '酒店位置'
  }
  state = {
    markers: [],
    latitude: 0,
    longitude: 0,
  }
  componentWillMount () { 
    const value = this.$router.params.id;
    if (value) {
      const result = JSON.parse(value);
      // console.log(result)
      let markers = []
      const object = {
        content: result.hotelname,
        padding: 15,
        bgColor: '#fff',
        fontSize: 14,
        color: '#000'
      }
      const marker = {
        id: 1,
        latitude: result.latitude,
        longitude: result.longitude,
        iconPath: 'https://www.uuwed.com/miniprogram/mark-icon.png',
        label: object
      }
      markers.push(marker)
      this.setState({
        latitude: result.latitude,
        longitude: result.longitude,
        markers: markers
      })
    }
  }

  componentWillReact () {
    //console.log('componentWillReact')
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const {latitude, longitude, markers} = this.state;
    return (
      <View className="page map-page" style={{ height: getWindowHeight(false, 0)}}>
        {
          process.env.TARO_ENV === 'weapp' ? 
          (
            <Map longitude={longitude} latitude={latitude} markers= {markers} className="hotel-map" />
          ) : null
        }
        
      </View>
    )
  }
}

export default MapPage
