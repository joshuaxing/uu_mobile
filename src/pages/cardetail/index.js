/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-29 15:40:00
 * @LastEditTime: 2019-08-21 16:51:49
 * @LastEditors: Please set LastEditors
 */
import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import Banner from './banner/index.js'
import './index.scss'
import http from '../../http/main.js'
let baseurl = 'https://www.uuwed.com/miniprogram'
@inject('counterStore')
@observer
class CarDetail extends Component {

  config = {
    navigationBarTitleText: '婚车详情'
  }
  state = {
    result: {},
    detailid: 0,
    bannerlist: [],
    otherlist: [],
    colorlist: [],
    userlist: [{
      name: '用作主婚车',
      id: 1
    }, {
      name: '用作礼宾车',
      id: 2
    }],
    datelist: [{
      name: '半天租用',
      id: 1
    }, {
      name: '整天租用',
      id: 2
    }],
    cardateid: 1,
    caruseid: 1,
    carcolorid: 1
  }
  componentWillMount () {
    const id = this.$router.params.id;
    this.setState({
      detailid: id
    }, () => {
      this.gotData()
    })
  }

  componentWillReact () {
    // console.log('componentWillReact')
  }

  componentDidMount () { }

  componentWillUnmount () {

  }

  componentDidShow () { }

  componentDidHide () { }
  gotData () {
    http('/app/carinfo.do', {
      id: this.state.detailid
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data;
        const value = {
          ...data,
          titleimage: data.pcpath[0]
        }
        let otherlist = data.pcpath.slice(1, data.pcpath.length);
        let colordata = data.colordata;
        this.setState({
          result: value,
          bannerlist: data.pcpath,
          otherlist: otherlist,
          colorlist: colordata,
          carcolorid: colordata[0].id
        })
      }
    })
  }
  changeUse (item) {
    this.setState({
      caruseid: item.id
    })
  }
  changeDate(item) {
    this.setState({
      cardateid: item.id
    })
  }
  changeColor(item) {
    this.setState({
      carcolorid: item.id
    })
  }
  prevImage (item) {
    const { otherlist } = this.state
    Taro.previewImage({
      current: item,
      urls: otherlist
    })
  }
  render () {
    const { result, bannerlist, otherlist, userlist, datelist, colorlist, cardateid, caruseid, carcolorid} = this.state;
    return (
      <View className="page cardetail-page">
        <View className="top-banner">
          <Banner list={bannerlist}></Banner>
        </View>
        <View className="header" >
          <View className="name">{result.name}</View>
           <View className='type-box'>
            <View className='type-color type-item'>
              车身颜色：
              {
                colorlist.map((item) => (
                  <View
                    key={item.id}
                    className = { carcolorid === item.id ? 'type-item-li type-item-active' : 'type-item-li'}
                    onClick = {this.changeColor.bind(this, item)}
                  >
                    {item.name}
                  </View>
                ))
              }
            </View>
            <View className='type-purpose type-item'>
              婚车用途：
              {
                userlist.map((item) => (
                  <View
                    key={item.id}
                    className = { caruseid === item.id ? 'type-item-li type-item-active' : 'type-item-li'}
                    onClick = {this.changeUse.bind(this, item)}
                  >
                    {item.name}
                  </View>
                ))
              }
            </View>
            <View className='type-time type-item'>
              租赁时间：
              {
                datelist.map((item) => (
                  <View
                    key={item.id}
                    className = { cardateid === item.id ? 'type-item-li type-item-active' : 'type-item-li'}
                    onClick = {this.changeDate.bind(this, item)}
                  >
                    {item.name}
                  </View>
                ))
              }
            </View>
          </View>
          {
            cardateid === 1 && (
              <View className="price">
                <Text className="price-name">婚车价格：</Text>
                <Text className="discount">￥{result.daydiscountmoney/2}</Text>
                <Text className="old">原价:￥{result.daymoney/2}</Text>
              </View>
            )
          }
          {
            cardateid === 2 && (
              <View className="price">
                <Text className="price-name">婚车价格：</Text>
                <Text className="discount">￥{result.daydiscountmoney}</Text>
                <Text className="old">原价:￥{result.daymoney}</Text>
              </View>
            )
          }
          <View className="info">
            {
              cardateid === 1 && (<Text className="info-txt">半天租用：{result.halfday}</Text>)
            }
            {
              cardateid === 2 && (<Text className="info-txt">整天租用：{result.wholeday}</Text>)
            }
          </View>
          {
            caruseid === 1 && (
              <View className="info">
                <Text className="info-txt">花环费用：￥{result.wreathprice}</Text>
              </View>
            )
          }
          {
            caruseid === 2 && (
              <View className="info">
                <Text className="info-txt">花环单价：￥0</Text>
              </View>
            )
          }
          <View className="introduce">
            {result.introduce}
          </View>

        </View>
        <View className="white-space"></View>
        <View className="flow-content">
          <View className="flow-title">
            <View className="icon"></View>
            <View className="title">预定流程</View>
            <View className="icon"></View>
          </View>
          <View className="flow-steps">
            <View className="flow-step">
              <Image src={`${baseurl}/flow1.png`} className="step-icon"/>
              <View className="step-name">提出需求</View>
            </View>
            <View className="flow-step">
              <Image src={`${baseurl}/flow2.png`} className="step-icon"/>
              <View className="step-name">确定路线</View>
            </View>
            <View className="flow-step">
              <Image src={`${baseurl}/flow3.png`} className="step-icon"/>
              <View className="step-name">签订合同</View>
            </View>
            <View className="flow-step">
              <Image src={`${baseurl}/flow4.png`} className="step-icon"/>
              <View className="step-name">等待服务</View>
            </View>
            <View className="flow-step">
              <Image src={`${baseurl}/flow5.png`} className="step-icon"/>
              <View className="step-name">完成服务</View>
            </View>
          </View>
        </View>
        <View className="white-space"></View>
        <View className="car-photo">
          <View className="flow-title">
            <View className="icon"></View>
            <View className="title">婚车相册</View>
            <View className="icon"></View>
          </View>
          <View className="photo-box">
            <View className="photo-items">
              {
                otherlist.map((item, index) => (
                  <View className="photo-item" key={item}>
                    <Image src={item} className="item-image" onClick={this.prevImage.bind(this, item)}/>
                  </View>
                ))
              }
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default CarDetail
