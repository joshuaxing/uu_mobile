import Taro, { Component } from '@tarojs/taro'
import { View, Text, ScrollView, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import './index.scss'
import Header from '../../components/header/index.js'
import Nodata from '../../components/nodata/index.js'
import { getWindowHeight } from '../../utils/style.js'
import http from '../../http/main.js'

@inject('counterStore')
@observer
class Product extends Component {

  config = {
    navigationBarTitleText: '婚庆产品',
    enablePullDownRefresh: true
  }
  navList = [
    {id: 0, name: '全部'},
    {id: 1, name: '最新'},
    {id: 2, name: '最热'},
  ]
  state = {
    totalpage: 0,
    page: 1,
    limit: 3,
    nodata: true,
    loading: false,
    hasMore: true,
    result: [],
    type: 0
  }

  componentWillMount () { 
    this.gotData()
  }

  componentWillReact () {
    this.setState({
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
    console.log('componentWillReact')
  }

  componentDidMount () { 
    
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  onPullDownRefresh () {
    this.setState({
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }

  handleType (item) {
    this.setState({
      type: item.id,
      result: [],
      page: 1,
      loading: false,
      hasMore: true,
      nodata: true
    }, () => {
      this.gotData();
    })
  }
  gotData () {
    //列表数据
    const { counterStore: { currentcity } } = this.props
    const { page, limit, hasMore, loading, type} = this.state;
    if (!hasMore || loading) {
      return
    }
    this.setState({ loading: true})
    http('/app/productlistbycity.do', {
      cityid: currentcity.id,
      areaid: currentcity.areaid,
      page: page,
      limit: limit,
      type: type
    })
    .then((result) => {
      if (result.status === 1) {
        const data = result.data.result;
        const total = result.data.total;
        const totalpage = Math.ceil(total / limit);
        const alldata = this.state.result.concat(data);
        if (alldata.length) {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: true
          })
        } else {
          this.setState({
            result: alldata,
            loading: false,
            totalpage: totalpage,
            nodata: false
          })
        }
        Taro.stopPullDownRefresh()
      }
    })
  }
  loadRecommend () {
    const {totalpage, page} = this.state;
    if (page >= totalpage) {
      this.setState({
        hasMore: false
      })
      return
    }
    this.setState({
      page: page + 1
    }, () => {
      this.gotData()
    })
  }
  handleDetail(item) {
    Taro.navigateTo({
      url: `/pages/productdetail/index?id=${item.id}`
    })
  }
  render () {
    const { result, type, nodata} = this.state;
    const { counterStore: { currentcity } } = this.props
    return (
      <View className="page product-page">
        <Header cityname={currentcity.name} areaname= {currentcity.areaname}></Header>
        <View className="page-content">
          <View className="page-title">
            {
              this.navList.map((item, index) => (
                <View 
                  className={item.id === type ? 'title title-active' : 'title'}
                  key = {item.id}
                  onClick = {this.handleType.bind(this,item)}
                >
                  {item.name}
                </View>
              ))
            }
          </View>
          <ScrollView 
            className="content-wrapper"
            scrollY
            style={{ height: getWindowHeight(true, 95)}}
            onScrollToLower={this.loadRecommend.bind(this)}
          > 
            <View className="table-box">
              {
                result.map((item, index) => (
                  <View className="table-li" key={item.id} onClick={this.handleDetail.bind(this, item)}>
                    <Image src={item.summarypc[0]} className="li-image"/>
                    <View className="li-content">
                      <View className="name">
                        <View className="name-title">{item.name}</View>
                        {
                          item.labelid === 1 &&
                          (<View className="name-lable">最新</View>) 
                        }
                        {
                          item.labelid === 2 &&
                          (<View className="name-lable">最热</View>)
                        }
                      </View>
                      <View className="price">
                        <View className="discount">￥<Text className="price-txt">{item.discountmoney}</Text></View>
                        <View className="old">原价:￥{item.money}</View>
                      </View>
                      <View className="lable">
                        <View className="lable-name">{item.characteristicname}</View>
                      </View>
                      <View className="des">{item.introduction}</View>
                    </View>
                  </View>
                ))
              }
              
            </View>
            {
              !nodata && <Nodata></Nodata>
            }
            {this.state.loading &&
              <View className='home__loading'>
                <Text className='home__loading-txt'>正在加载中...</Text>
              </View>
            }
            {!this.state.hasMore &&
              <View className='home__loading home__loading--not-more'>
                <Text className='home__loading-txt'>没有更多内容了</Text>
              </View>
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

export default Product
