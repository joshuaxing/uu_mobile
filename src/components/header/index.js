import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import adIcon from './ad_icon.png'
import './index.scss'


@inject('counterStore')
@observer
class Header extends Component {
  static defaultProps = {
    cityname: '',
    areaname: ''
  }
  

  componentWillReact () {
   
  }

  componentDidMount () {
    
    
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  goReturn () {
    Taro.navigateTo({
      url: '/pages/city/index'
    })
  }
  render () {
    const { cityname, areaname } = this.props;
    return (
      <View className="page-header">
        <View className="page-header-left" onClick={this.goReturn}>
          <Image src={adIcon} className="ad-icon"/>
          <Text className="name">{cityname}-{areaname}</Text>
          <Text className="name ad-name">[切换]</Text>
        </View>
      </View>
    )
  }
}

export default Header
