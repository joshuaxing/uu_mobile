import Taro, { Component } from '@tarojs/taro'
import { View, Image} from '@tarojs/components'
import { observer, inject } from '@tarojs/mobx'
import noIcon from './nodata_icon.png'
import './index.scss'


@inject('counterStore')
@observer
class Header extends Component {
  static defaultProps = {
   
  }
  

  componentWillReact () {
   
  }

  componentDidMount () {
    
    
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {

    return (
      <View className="page-nodata">
        <Image src={noIcon} className="nodata-icon"/>
        <View className="nodata-text">暂时没有数据</View>
      </View>
    )
  }
}

export default Header
