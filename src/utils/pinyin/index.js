import pinyin_dict_firstletter from './pinyin_dict_firstletter.js'

export default function getFirstLetter(str) {
  if (!str || /^ +$/g.test(str)) return '';
  var result = [];
  for (var i = 0; i < str.length; i++) {
    var unicode = str.charCodeAt(i);
    var ch = str.charAt(i);
    if (unicode >= 19968 && unicode <= 40869) {
      ch = pinyin_dict_firstletter.all.charAt(unicode - 19968);
    }
    result.push(ch);
  }
  return result.join(''); // 如果不用管多音字，直接将数组拼接成字符串
}