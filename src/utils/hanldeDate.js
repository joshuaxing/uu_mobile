export function formatDate(timestamp, type = 1) {
    let date = new Date(timestamp);
    let year = date.getFullYear(),
      month = date.getMonth() + 1, //月份是从0开始的
      day = date.getDate(),
      hour = date.getHours(),
      min = date.getMinutes(),
      sec = date.getSeconds();
      if (type === 1) {
        return year + '-' + fillZero(month) + '-' + fillZero(day)
      } else if(type === 2){
        return fillZero(month) + '月' + fillZero(day) + '日'
      } else if (type === 3) {
        return fillZero(month) + '.' + fillZero(day)
      }
  }
  
  export function formatWeek(timestamp) {
    let date = new Date(timestamp);
    let week = date.getDay();
    let weekChinese = ["日", "一", "二", "三", "四", "五", "六"]
    return weekChinese[week]
  }
  
  function fillZero (num) {
    return num < 10 ? '0'+ num : num
  }
  
  