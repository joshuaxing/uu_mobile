
/* eslint-disable */
export const host = HOST
/* eslint-enable */
const method = 'post'
import fetch from './fetch.js'
/**
 * 优品API
 * @param  {Objece} params 参数
 * @return {Promise}       包含抓取任务的Promise
 */
export default (path, params) => {
  return fetch(host, path, method, params)
}