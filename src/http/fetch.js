import Taro, { Component } from '@tarojs/taro'
/**
 * 获取数据API的结构
 * http: //sh.uuwed.com/
 * @param  {String} url    api 根地址
 * @param  {String} path   请求路径
 * @param  {Objece} params 参数
 * @return {Promise}       包含抓取任务的Promise
 */
let other = {
  app_platform: 3,
  app_version: 1
}
import md5 from '../utils/handleSign.js'
const fetch = (url, path, method, params) => {
  let data = Object.assign({}, params, other)
  data.app_sign = md5(data);
  return new Promise((resolve, reject) => {
    Taro.request({
      url: `${url}${path}`,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }).then((response) => {
      resolve(response.data)
    }).catch((error) => {
      reject(error)
    })
  })
}

export default fetch