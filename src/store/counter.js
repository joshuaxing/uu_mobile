import { observable } from 'mobx'

const counterStore = observable({
  currentcity: {
    areaname: '全城',
    areaid: 0,
    name: '上海',
    id: 793
  },
  user: {
    account: '',
    id: 0
  },
  gotCurrentCity(data) {
    this.currentcity = data;
  },
  gotUserInfo(data) {
    this.user = data;
  }
})
export default counterStore