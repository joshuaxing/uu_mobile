const HOST = '"https://www.uuwed.com"'
const HOSTH5 = '"http://m.h5.uuwed.com"'
const isH5 = process.env.CLIENT_ENV === 'h5'
module.exports = {
  env: {
    NODE_ENV: '"production"'
  },
  defineConstants: {
    HOST: isH5 ? HOSTH5 : HOST
  },
  weapp: {},
  h5: {
    /**
     * 如果h5端编译后体积过大，可以使用webpack-bundle-analyzer插件对打包体积进行分析。
     * 参考代码如下：
     * webpackChain (chain) {
     *   chain.plugin('analyzer')
     *     .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])
     * }
     */
    publicPath: '/',
    router: {
      mode: 'browser'
    }
  }
}
